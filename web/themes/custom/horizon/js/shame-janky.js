(function($) {

  /**
   * Add/remove the classes to adjust the navbar to account for the admin menu
   * @returns {null} No return
   */
  const addClassToAdjustAdminNav = function() {
    if ($('body.user-logged-in.toolbar-tray-open.toolbar-fixed.toolbar-horizontal').length) {
      $('.navbar-static-top,.dropdown-menu.nav-second-tier').addClass('adjust-nav-with-admin-menu-horizontal');
      $('.navbar-static-top,.dropdown-menu.nav-second-tier').removeClass('adjust-nav-with-admin-menu-vertical');
    } else if ($('body.user-logged-in.toolbar-tray-open.toolbar-fixed.toolbar-vertical').length) {
      $('.navbar-static-top,.dropdown-menu.nav-second-tier').addClass('adjust-nav-with-admin-menu-vertical');
      $('.navbar-static-top,.dropdown-menu.nav-second-tier').removeClass('adjust-nav-with-admin-menu-horizontal');
    }
  };

  $(document).ready(function() {
    // GCBIA-671: Adjust the fixed positioning of the header when the admin
    // menu is present
    addClassToAdjustAdminNav();

    $('.toolbar-lining').on('click', function() {
      // We need to use setTimeout here so that this doesn't run before the classes have been updated
      setTimeout(addClassToAdjustAdminNav, 200);
    });
  });

})(jQuery);
