<?php
require_once 'web/themes/contrib/bootstrap/src/Bootstrap.php';
require_once 'src/themes/horizon/src/Horizon.php';

class HorizonTest extends PHPUnit_Framework_TestCase {
  private $helper;
  private $mockHelper;

  /**
   * @beforeClass
   */
  public function setup() {
    $this->helper = Drupal\horizon\Horizon::getInstance();
    $this->mockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                             ->setMethods(array('createNewAttribute', 'createMarkup', 'fromUri', 'drupalFileCreateUrl', 'drupalFileLoad', 'drupalGetRequestUri', 'getDefaultLegalFooterContent', 'getDefaultFooterLinks', 'drupalT', 'drupalThemeGetSetting', 'drupalFileDefaultScheme', 'drupalGetConfig', 'drupalNodeLoad'))
                             ->disableOriginalConstructor()
                             ->getMock();
  }

  public function testAddLibraries_local() {
    $libraries = array();
    $environment = 'local';

    $result = $this->helper->addLibraries($libraries, $environment);
    $this->assertCount(4, $result);
    $this->assertEquals('horizon/horizon-prod-css', $result[0]);
    $this->assertEquals('horizon/horizon-prod-js', $result[1]);
    $this->assertEquals('horizon/horizon-theme-css', $result[2]);
    $this->assertEquals('horizon/horizon-theme-js', $result[3]);
  }

  public function testAddLibraries_test() {
    $libraries = array();
    $environment = 'test';

    $result = $this->helper->addLibraries($libraries, $environment);
    $this->assertCount(4, $result);
    $this->assertEquals('horizon/horizon-test-css', $result[0]);
    $this->assertEquals('horizon/horizon-test-js', $result[1]);
    $this->assertEquals('horizon/horizon-theme-css', $result[2]);
    $this->assertEquals('horizon/horizon-theme-js', $result[3]);
  }

  public function testAddLibraries_pilot() {
    $libraries = array();
    $environment = 'pilot';

    $result = $this->helper->addLibraries($libraries, $environment);
    $this->assertCount(4, $result);
    $this->assertEquals('horizon/horizon-pilot-css', $result[0]);
    $this->assertEquals('horizon/horizon-pilot-js', $result[1]);
    $this->assertEquals('horizon/horizon-theme-css', $result[2]);
    $this->assertEquals('horizon/horizon-theme-js', $result[3]);
  }

  public function testAddLibraries_prodWithExistingLibrariesArray() {
    $libraries = array('value1', 'value2');
    $environment = 'prod';

    $result = $this->helper->addLibraries($libraries, $environment);
    $this->assertCount(6, $result);
    $this->assertEquals('value1', $result[0]);
    $this->assertEquals('value2', $result[1]);
    $this->assertEquals('horizon/horizon-prod-css', $result[2]);
    $this->assertEquals('horizon/horizon-prod-js', $result[3]);
    $this->assertEquals('horizon/horizon-theme-css', $result[4]);
    $this->assertEquals('horizon/horizon-theme-js', $result[5]);
  }

  public function testPreprocessFormElementLabel_textfieldRequiredTitleBefore() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#title'] = 'element title';
    $variables['element']['#type'] = 'textfield';
    $variables['element']['#title_display'] = 'before';
    $variables['element']['#required'] = true;
    $variables['element']['#id'] = 'element_id';

    $attributes = array();
    $attributes['class'] = array('control-label', 'is-required');
    $attributes['for'] = 'element_id';

    $this->mockHelper->expects($this->once())
                     ->method('createNewAttribute')
                     ->with($attributes)
                     ->will($this->returnValue('Created new attributes'));

    $result = $this->mockHelper->preprocessFormElementLabel($variables);
    $this->assertEquals('Created new attributes', $result['attributes']);
    $this->assertEquals('element title', $result['text']);
    $this->assertEquals('element_id', $result['element_id']);
  }

  public function testPreprocessFormElementLabel_textareaNotRequiredTitleAfter() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#title'] = 'element title';
    $variables['element']['#type'] = 'textarea';
    $variables['element']['#title_display'] = 'after';
    $variables['element']['#required'] = false;
    $variables['element']['#id'] = 'element_id';

    $attributes = array();
    $attributes['class'] = array('textarea', 'control-label');
    $attributes['for'] = 'element_id';

    $this->mockHelper->expects($this->once())
                     ->method('createNewAttribute')
                     ->with($attributes)
                     ->will($this->returnValue('Created new attributes'));

    $result = $this->mockHelper->preprocessFormElementLabel($variables);
    $this->assertEquals('Created new attributes', $result['attributes']);
    $this->assertEquals('element title', $result['text']);
    $this->assertEquals('element_id', $result['element_id']);
  }

  public function testPreprocessFormElementLabel_radioRequiredTitleInvisible() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#title'] = 'element title';
    $variables['element']['#type'] = 'radio';
    $variables['element']['#title_display'] = 'invisible';
    $variables['element']['#required'] = true;
    $variables['element']['#id'] = 'element_id';

    $attributes = array();
    $attributes['class'] = array('sr-only', 'control-label', 'is-required');
    $attributes['for'] = 'element_id';

    $this->mockHelper->expects($this->once())
                     ->method('createNewAttribute')
                     ->with($attributes)
                     ->will($this->returnValue('Created new attributes'));

    $result = $this->mockHelper->preprocessFormElementLabel($variables);
    $this->assertEquals('Created new attributes', $result['attributes']);
    $this->assertEquals('element title', $result['text']);
    $this->assertEquals('element_id', $result['element_id']);
  }

  public function testPreprocessFormElementLabel_checkboxRequiredTitleAfter() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#title'] = 'element title';
    $variables['element']['#type'] = 'checkbox';
    $variables['element']['#title_display'] = 'after';
    $variables['element']['#required'] = true;
    $variables['element']['#id'] = 'element_id';

    $attributes = array();
    $attributes['class'] = array('control-label', 'is-required');
    $attributes['for'] = 'element_id';

    $this->mockHelper->expects($this->once())
                     ->method('createNewAttribute')
                     ->with($attributes)
                     ->will($this->returnValue('Created new attributes'));

    $result = $this->mockHelper->preprocessFormElementLabel($variables);
    $this->assertEquals('Created new attributes', $result['attributes']);
    $this->assertEquals('element title', $result['text']);
    $this->assertEquals('element_id', $result['element_id']);
  }

  public function testPreprocessFormElementLabel_textfieldTitleAndRequiredBothEmpty() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#title'] = '';
    $variables['element']['#type'] = 'textfield';
    $variables['element']['#title_display'] = 'after';
    $variables['element']['#required'] = '';
    $variables['element']['#id'] = 'element_id';

    $this->mockHelper->expects($this->never())
                     ->method('createNewAttribute');

    $result = $this->mockHelper->preprocessFormElementLabel($variables);
    $this->assertEquals('', $result);
  }

  public function testPreprocessCheckboxes_variablesAttributesNotSet_hasInlineClass() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#attributes']['class'] = array('element-class-one', 'element-class-two');
    $variables['element']['#attributes']['aria-describedby'] = 'element-describedby';
    $variables['element']['#children'] = array('child-one', 'control-label child-two', 'child-three');
    $variables['element']['#options_display'] = 'element options display';

    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('matchInline'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $matchInlineCalls = array(
      array('form-checkboxes', false),
      array('element-class-one', true),
      array('element-class-two', false)
    );

    $myMockHelper->expects($this->exactly(3))
                 ->method('matchInline')
                 ->will($this->returnValueMap($matchInlineCalls));

    $expectedResult = $variables;
    $expectedResult['attributes']['class'] = array();
    $expectedResult['attributes']['class'][0] = 'form-checkboxes';
    $expectedResult['attributes']['class'][2] = 'element-class-two';
    $expectedResult['attributes']['aria-describedby'] = 'element-describedby';
    $expectedResult['element']['#children'] = array('child-one', 'control-label element-class-one child-two', 'child-three');
    $expectedResult['display_type'] = 'element options display';

    $result = $myMockHelper->preprocessCheckboxes($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testPreprocessCheckboxes_variablesAttributesSet_noElementAttributes_noDescribedBy_noInlineClass() {
    $variables = array();
    $variables['element'] = array();
    $variables['attributes'] = array();
    $variables['attributes']['class'] = array('first-class');
    $variables['element']['#attributes']['class'] = array();
    $variables['element']['#children'] = array('child-one', 'control-label child-two', 'child-three');
    $variables['element']['#options_display'] = 'element options display';

    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('matchInline'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $matchInlineCalls = array(
      array('first-class', false),
      array('form-checkboxes', false)
    );

    $myMockHelper->expects($this->exactly(2))
                 ->method('matchInline')
                 ->will($this->returnValueMap($matchInlineCalls));

    $expectedResult = $variables;
    $expectedResult['attributes']['class'] = array();
    $expectedResult['attributes']['class'][0] = 'first-class';
    $expectedResult['attributes']['class'][1] = 'form-checkboxes';
    $expectedResult['element']['#children'] = array('child-one', 'control-label child-two', 'child-three');
    $expectedResult['display_type'] = 'element options display';

    $result = $myMockHelper->preprocessCheckboxes($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testReplaceDefaultDescriptionHtml_happy() {
    $variables = array();
    $variables['element'] = array();
    $variables['field-to-overwrite'] = '<div class="description">Some description text</div>';
    $variables['element']['#attributes']['data-drupal-selector'] = 'drupal-selector-orig';
    $variablesKey = 'field-to-overwrite';

    $this->mockHelper->expects($this->once())
                     ->method('createMarkup')
                     ->with('<span id="drupal-selector-orig--description" class="help-block">Some description text</span>')
                     ->will($this->returnValue('Created markup for div html.'));

    $expectedResult = $variables;
    $expectedResult['field-to-overwrite'] = 'Created markup for div html.';
    $expectedResult['attributes']['aria-describedby'] = 'drupal-selector-orig--description';

    $result = $this->mockHelper->replaceDefaultDescriptionHtml($variables, $variablesKey);
    $this->assertSame($expectedResult, $result);
  }

  public function testReplaceDefaultDescriptionHtml_sad_noVariablesKey() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#attributes']['data-drupal-selector'] = 'drupal-selector-orig';
    $variablesKey = 'field-to-overwrite';

    $this->mockHelper->expects($this->never())
                     ->method('createMarkup');

    $expectedResult = $variables;
    $expectedResult['attributes']['aria-describedby'] = 'drupal-selector-orig--description';

    $result = $this->mockHelper->replaceDefaultDescriptionHtml($variables, $variablesKey);
    $this->assertSame($expectedResult, $result);
  }

  public function testReplaceDefaultDescriptionHtml_sad_noDescription() {
    $variables = array();
    $variables['element'] = array();
    $variables['field-to-overwrite'] = 'Ain\'t no divs on me';
    $variables['element']['#attributes']['data-drupal-selector'] = 'drupal-selector-orig';
    $variablesKey = 'field-to-overwrite';

    $this->mockHelper->expects($this->never())
                     ->method('createMarkup');

    $expectedResult = $variables;
    $expectedResult['attributes']['aria-describedby'] = 'drupal-selector-orig--description';

    $result = $this->mockHelper->replaceDefaultDescriptionHtml($variables, $variablesKey);
    $this->assertSame($expectedResult, $result);
  }

  public function testSetCtaVariables_happy() {
    $variables = array();
    // Setup First CTA
    $mockCta1 = $this->getMockBuilder('MockEntity')
                     ->setConstructorArgs(array())
                     ->getMock();
    $bodyField1 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $bodyFieldArray1 = array(array('value' => 'some body text for cta 1'));
    $bodyField1->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($bodyFieldArray1));
    $titleField1 = $this->getMockBuilder('MockField')
                        ->setConstructorArgs(array())
                        ->getMock();
    $titleFieldArray1 = array(array('value' => 'title for cta 1'));
    $titleField1->expects($this->once())
                ->method('getValue')
                ->will($this->returnValue($titleFieldArray1));
    $linkField1 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $linkFieldArray1 = array(array('uri' => 'link_url_1', 'title' => 'link_title_1'));
    $linkField1->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($linkFieldArray1));
    $cta1GetCallsMap = [
      ['body', $bodyField1],
      ['title', $titleField1],
      ['field_cta_link', $linkField1]
    ];
    $mockCta1->expects($this->exactly(3))
                    ->method('get')
                    ->will($this->returnValueMap($cta1GetCallsMap));
    // Setup Second CTA
    $mockCta2 = $this->getMockBuilder('MockEntity')
                     ->setConstructorArgs(array())
                     ->getMock();
    $bodyField2 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $bodyFieldArray2 = array(array('value' => 'some body text for cta 2'));
    $bodyField2->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($bodyFieldArray2));
    $titleField2 = $this->getMockBuilder('MockField')
                        ->setConstructorArgs(array())
                        ->getMock();
    $titleFieldArray2 = array(array('value' => 'title for cta 2'));
    $titleField2->expects($this->once())
                ->method('getValue')
                ->will($this->returnValue($titleFieldArray2));
    $linkField2 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $linkFieldArray2 = array(array('uri' => 'link_url_2', 'title' => 'link_title_2'));
    $linkField2->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($linkFieldArray2));
    $cta2GetCallsMap = [
      ['body', $bodyField2],
      ['title', $titleField2],
      ['field_cta_link', $linkField2]
    ];
    $mockCta2->expects($this->exactly(3))
                    ->method('get')
                    ->will($this->returnValueMap($cta2GetCallsMap));
    // Setup Third CTA
    $mockCta3 = $this->getMockBuilder('MockEntity')
                     ->setConstructorArgs(array())
                     ->getMock();
    $bodyField3 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $bodyFieldArray3 = array(array('value' => 'some body text for cta 3'));
    $bodyField3->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($bodyFieldArray3));
    $titleField3 = $this->getMockBuilder('MockField')
                        ->setConstructorArgs(array())
                        ->getMock();
    $titleFieldArray3 = array(array('value' => 'title for cta 3'));
    $titleField3->expects($this->once())
                ->method('getValue')
                ->will($this->returnValue($titleFieldArray3));
    $linkField3 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $linkFieldArray3 = array(array('uri' => 'link_url_3', 'title' => 'link_title_3'));
    $linkField3->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($linkFieldArray3));
    $cta3GetCallsMap = [
      ['body', $bodyField3],
      ['title', $titleField3],
      ['field_cta_link', $linkField3]
    ];
    $mockCta3->expects($this->exactly(3))
                    ->method('get')
                    ->will($this->returnValueMap($cta3GetCallsMap));

    $ctas = array($mockCta1, $mockCta2, $mockCta3);

    $mockItemsEntity = $this->getMockBuilder('MockEntity')
                            ->setConstructorArgs(array())
                            ->getMock();

    $mockItemsEntity->expects($this->once())
                    ->method('referencedEntities')
                    ->will($this->returnValue($ctas));

    $variables['element']['#items'] = $mockItemsEntity;

    $urlObj1 = $this->getMockBuilder('MockUrl')
                    ->setConstructorArgs(array())
                    ->getMock();
    $urlObj1->expects($this->once())
            ->method('toString')
            ->will($this->returnValue('String of url 1'));
    $urlObj2 = $this->getMockBuilder('MockUrl')
                    ->setConstructorArgs(array())
                    ->getMock();
    $urlObj2->expects($this->once())
            ->method('toString')
            ->will($this->returnValue('String of url 2'));
    $urlObj3 = $this->getMockBuilder('MockUrl')
                    ->setConstructorArgs(array())
                    ->getMock();
    $urlObj3->expects($this->once())
            ->method('toString')
            ->will($this->returnValue('String of url 3'));

    $fromUriCallsMap = [
      ['link_url_1', array(), $urlObj1],
      ['link_url_2', array(), $urlObj2],
      ['link_url_3', array(), $urlObj3]
    ];

    $this->mockHelper->expects($this->exactly(3))
                     ->method('fromUri')
                     ->will($this->returnValueMap($fromUriCallsMap));

    $expectedResult = $variables;
    $expectedCtas = array();
    $expectedCtas[0] = array('body' => 'some body text for cta 1', 'title' => 'title for cta 1', 'link_url' => 'String of url 1', 'link_title' => 'link_title_1');
    $expectedCtas[1] = array('body' => 'some body text for cta 2', 'title' => 'title for cta 2', 'link_url' => 'String of url 2', 'link_title' => 'link_title_2');
    $expectedCtas[2] = array('body' => 'some body text for cta 3', 'title' => 'title for cta 3', 'link_url' => 'String of url 3', 'link_title' => 'link_title_3');
    $expectedResult['ctas'] = $expectedCtas;

    $this->mockHelper->setCtaVariables($variables);
    $this->assertSame($expectedResult, $variables);
  }

  public function testSetCtaVariables_linkFieldArrayEmpty() {
    $variables = array();
    // Setup First CTA
    $mockCta1 = $this->getMockBuilder('MockEntity')
                     ->setConstructorArgs(array())
                     ->getMock();
    $bodyField1 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $bodyFieldArray1 = array(array('value' => 'some body text for cta 1'));
    $bodyField1->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue($bodyFieldArray1));
    $titleField1 = $this->getMockBuilder('MockField')
                        ->setConstructorArgs(array())
                        ->getMock();
    $titleFieldArray1 = array(array('value' => 'title for cta 1'));
    $titleField1->expects($this->once())
                ->method('getValue')
                ->will($this->returnValue($titleFieldArray1));
    $linkField1 = $this->getMockBuilder('MockField')
                       ->setConstructorArgs(array())
                       ->getMock();
    $linkField1->expects($this->once())
               ->method('getValue')
               ->will($this->returnValue(array()));
    $cta1GetCallsMap = [
      ['body', $bodyField1],
      ['title', $titleField1],
      ['field_cta_link', $linkField1]
    ];
    $mockCta1->expects($this->exactly(3))
                    ->method('get')
                    ->will($this->returnValueMap($cta1GetCallsMap));

    $ctas = array($mockCta1);

    $mockItemsEntity = $this->getMockBuilder('MockEntity')
                            ->setConstructorArgs(array())
                            ->getMock();

    $mockItemsEntity->expects($this->once())
                    ->method('referencedEntities')
                    ->will($this->returnValue($ctas));

    $variables['element']['#items'] = $mockItemsEntity;

    $this->mockHelper->expects($this->never())
                     ->method('fromUri');

    $expectedResult = $variables;
    $expectedCtas = array();
    $expectedCtas[0] = array('body' => 'some body text for cta 1', 'title' => 'title for cta 1', 'link_url' => '', 'link_title' => '');
    $expectedResult['ctas'] = $expectedCtas;

    $this->mockHelper->setCtaVariables($variables);
    $this->assertSame($expectedResult, $variables);
  }

  public function testSetCtaVariables_noCTAs() {
    $variables = array();
    $ctas = array();

    $mockItemsEntity = $this->getMockBuilder('MockEntity')
                            ->setConstructorArgs(array())
                            ->getMock();

    $mockItemsEntity->expects($this->once())
                    ->method('referencedEntities')
                    ->will($this->returnValue($ctas));

    $variables['element']['#items'] = $mockItemsEntity;

    $this->mockHelper->expects($this->never())
                     ->method('fromUri');

    $expectedResult = $variables;
    $expectedResult['ctas'] = array();

    $this->mockHelper->setCtaVariables($variables);
    $this->assertSame($expectedResult, $variables);
  }

  public function testSetHeroVariables_rightInverse() {
    $variables = array();
    $uri = $this->getMockBuilder('MockField')
                ->setConstructorArgs(array())
                ->getMock();
    $uri->expects($this->once())
        ->method('getValue')
        ->will($this->returnValue(array(array('value' => 'hero_image_uri'))));

    $mockHeroImg = $this->getMockBuilder('MockFile')
                        ->setConstructorArgs(array($uri))
                        ->getMock();

    $mockElementObjectEntity = $this->getMockBuilder('MockEntity')
                                    ->setConstructorArgs(array())
                                    ->getMock();

    $nodeArray = array();
    $nodeArray['field_hero_image_full_width'][0]['target_id'] = 'full_width_target_id';
    $nodeArray['field_hero_title'][0]['value'] = 'My snazzy hero title';
    $nodeArray['field_hero_body'][0]['value'] = 'My non-marked up description';
    $nodeArray['field_hero_cta'][0]['uri'] = 'cta_uri';
    $nodeArray['field_hero_cta'][0]['title'] = 'cta_title';
    $nodeArray['field_hero_alignment'][0]['value'] = 'right';
    $nodeArray['field_hero_text_color'][0]['value'] = 'light';

    $mockElementObjectEntity->expects($this->once())
                            ->method('toArray')
                            ->will($this->returnValue($nodeArray));

    $this->mockHelper->expects($this->once())
                     ->method('drupalFileLoad')
                     ->with('full_width_target_id')
                     ->will($this->returnValue($mockHeroImg));

    $this->mockHelper->expects($this->once())
                     ->method('drupalFileCreateUrl')
                     ->with('hero_image_uri')
                     ->will($this->returnValue('hero_image_url'));

    $this->mockHelper->expects($this->once())
                     ->method('createMarkup')
                     ->with('My non-marked up description')
                     ->will($this->returnValue('My marked up description'));

    $variables['element']['#object'] = $mockElementObjectEntity;

    $expectedResult = $variables;
    $expectedResult['hero_headline'] = 'My snazzy hero title';
    $expectedResult['hero_image_path'] = 'hero_image_url';
    $expectedResult['hero_description'] = 'My marked up description';
    $expectedResult['hero_cta_link'] = 'cta_uri';
    $expectedResult['hero_cta_title'] = 'cta_title';
    $expectedResult['hero_copy_alignment_class'] = 'hero-copy-right';
    $expectedResult['hero_inverse_class'] = 'hero-inverse';

    $this->mockHelper->setHeroVariables($variables);
    $this->assertSame($expectedResult, $variables);
  }

  public function testSetHeroVariables_leftNormal() {
    $variables = array();
    $uri = $this->getMockBuilder('MockField')
                ->setConstructorArgs(array())
                ->getMock();
    $uri->expects($this->once())
        ->method('getValue')
        ->will($this->returnValue(array(array('value' => 'hero_image_uri'))));

    $mockHeroImg = $this->getMockBuilder('MockFile')
                        ->setConstructorArgs(array($uri))
                        ->getMock();

    $mockElementObjectEntity = $this->getMockBuilder('MockEntity')
                                    ->setConstructorArgs(array())
                                    ->getMock();

    $nodeArray = array();
    $nodeArray['field_hero_image_full_width'][0]['target_id'] = 'full_width_target_id';
    $nodeArray['field_hero_title'][0]['value'] = 'My snazzy hero title';
    $nodeArray['field_hero_body'][0]['value'] = 'My non-marked up description';
    $nodeArray['field_hero_cta'][0]['uri'] = 'cta_uri';
    $nodeArray['field_hero_cta'][0]['title'] = 'cta_title';
    $nodeArray['field_hero_alignment'][0]['value'] = 'left';
    $nodeArray['field_hero_text_color'][0]['value'] = 'notlight';

    $mockElementObjectEntity->expects($this->once())
                            ->method('toArray')
                            ->will($this->returnValue($nodeArray));

    $this->mockHelper->expects($this->once())
                     ->method('drupalFileLoad')
                     ->with('full_width_target_id')
                     ->will($this->returnValue($mockHeroImg));

    $this->mockHelper->expects($this->once())
                     ->method('drupalFileCreateUrl')
                     ->with('hero_image_uri')
                     ->will($this->returnValue('hero_image_url'));

    $this->mockHelper->expects($this->once())
                     ->method('createMarkup')
                     ->with('My non-marked up description')
                     ->will($this->returnValue('My marked up description'));

    $variables['element']['#object'] = $mockElementObjectEntity;

    $expectedResult = $variables;
    $expectedResult['hero_headline'] = 'My snazzy hero title';
    $expectedResult['hero_image_path'] = 'hero_image_url';
    $expectedResult['hero_description'] = 'My marked up description';
    $expectedResult['hero_cta_link'] = 'cta_uri';
    $expectedResult['hero_cta_title'] = 'cta_title';
    $expectedResult['hero_copy_alignment_class'] = 'hero-copy-left';
    $expectedResult['hero_inverse_class'] = '';

    $this->mockHelper->setHeroVariables($variables);
    $this->assertSame($expectedResult, $variables);
  }

  public function testAddActiveClasses_parentLinkEmpty_urlIsCurrentPath() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $link = array('url' => 'current path');

    $expectedResult = array();
    $expectedResult['tier1'] = array('url' => 'current path', 'current_page_classes' => 'current-page tier-one-active-page');
    $expectedResult['tier2'] = array();

    $result = $this->helper->addActiveClasses($currentPath, $currentPathAlias, $link);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddActiveClasses_parentLinkEmpty_urlIsCurrentPathAlias() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $link = array('url' => 'current path alias');

    $expectedResult = array();
    $expectedResult['tier1'] = array('url' => 'current path alias', 'current_page_classes' => 'current-page tier-one-active-page');
    $expectedResult['tier2'] = array();

    $result = $this->helper->addActiveClasses($currentPath, $currentPathAlias, $link);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddActiveClasses_parentLinkEmpty_urlNotCurrentPath() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $link = array('url' => 'some other path');

    $expectedResult = array();
    $expectedResult['tier1'] = array('url' => 'some other path');
    $expectedResult['tier2'] = array();

    $result = $this->helper->addActiveClasses($currentPath, $currentPathAlias, $link);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddActiveClasses_parentLinkExists_urlIsCurrentPath() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $link = array('url' => 'test.comcurrent path');
    $parentLink = array('url' => 'parent link url');

    $expectedResult = array();
    $expectedResult['tier1'] = array('url' => 'parent link url', 'current_page_classes' => 'current-page tier-one-active-page');
    $expectedResult['tier2'] = array('url' => 'test.comcurrent path', 'current_page_classes' => 'active-trail tier-two-active-page');

    $result = $this->helper->addActiveClasses($currentPath, $currentPathAlias, $link, $parentLink);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddActiveClasses_parentLinkExists_urlIsCurrentPathAlias() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $link = array('url' => 'test.comcurrent path alias');
    $parentLink = array('url' => 'parent link url');

    $expectedResult = array();
    $expectedResult['tier1'] = array('url' => 'parent link url', 'current_page_classes' => 'current-page tier-one-active-page');
    $expectedResult['tier2'] = array('url' => 'test.comcurrent path alias', 'current_page_classes' => 'active-trail tier-two-active-page');

    $result = $this->helper->addActiveClasses($currentPath, $currentPathAlias, $link, $parentLink);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddActiveClasses_parentLinkExists_urlNotCurrentPath() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $link = array('url' => 'test.comsome other path');
    $parentLink = array('url' => 'parent link url');

    $expectedResult = array();
    $expectedResult['tier1'] = array('url' => 'parent link url');
    $expectedResult['tier2'] = array('url' => 'test.comsome other path');

    $result = $this->helper->addActiveClasses($currentPath, $currentPathAlias, $link, $parentLink);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddClassesForNonMenuPages_classAlreadySet() {
    $currentPath = 'current path';
    $currentPathAlias = 'current path alias';
    $tier1HeaderLinks = array();
    $tier1HeaderLinks[0] = array('current_page_classes' => 'blah');
    $tier1HeaderLinks[1] = array('current_page_classes' => 'blah');
    $tier1HeaderLinks[2] = array('current_page_classes' => 'current-page tier-one-active-page');
    $tier1HeaderLinks[3] = array('current_page_classes' => 'blah');
    $tier1HeaderLinks[4] = array('current_page_classes' => 'blah');

    $expectedResult = $tier1HeaderLinks;

    $result = $this->helper->addClassesForNonMenuPages($currentPath, $currentPathAlias, $tier1HeaderLinks);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddClassesForNonMenuPages_classNotSetButExistsInPath() {
    $currentPath = '/employers/whigs';
    $currentPathAlias = '/individuals/blah';
    $tier1HeaderLinks = array();

    $tier1HeaderLinks[0] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/wisconsin');
    $tier1HeaderLinks[1] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');
    $tier1HeaderLinks[2] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/employers');
    $tier1HeaderLinks[3] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');
    $tier1HeaderLinks[4] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');

    $expectedResult = $tier1HeaderLinks;
    $expectedResult[2]['current_page_classes'] = 'current-page tier-one-active-page';

    $result = $this->helper->addClassesForNonMenuPages($currentPath, $currentPathAlias, $tier1HeaderLinks);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddClassesForNonMenuPages_classNotSetButExistsInPathAlias() {
    $currentPath = '/individuals/blah';
    $currentPathAlias = '/employers/whigs';
    $tier1HeaderLinks = array();

    $tier1HeaderLinks[0] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/wisconsin');
    $tier1HeaderLinks[1] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');
    $tier1HeaderLinks[2] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/employers');
    $tier1HeaderLinks[3] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');
    $tier1HeaderLinks[4] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');

    $expectedResult = $tier1HeaderLinks;
    $expectedResult[2]['current_page_classes'] = 'current-page tier-one-active-page';

    $result = $this->helper->addClassesForNonMenuPages($currentPath, $currentPathAlias, $tier1HeaderLinks);
    $this->assertSame($expectedResult, $result);
  }

  public function testAddClassesForNonMenuPages_classNotSetDoesNotExistInPath() {
    $currentPath = '/individuals/blah';
    $currentPathAlias = '/employers/whigs';
    $tier1HeaderLinks = array();

    $tier1HeaderLinks[0] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/wisconsin');
    $tier1HeaderLinks[1] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');
    $tier1HeaderLinks[2] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/iowa');
    $tier1HeaderLinks[3] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');
    $tier1HeaderLinks[4] = array('current_page_classes' => 'blah', 'url' => 'http://www.blah.com/blah');

    $expectedResult = $tier1HeaderLinks;
    $expectedResult[0]['current_page_classes'] = 'current-page tier-one-active-page';

    $result = $this->helper->addClassesForNonMenuPages($currentPath, $currentPathAlias, $tier1HeaderLinks);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetHeaderMenuLinks_happy() {
    // Every setting has a value...here we go!
    $currentPath = 'current';
    $currentPathAlias = 'current alias';

    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('getLeftTier1Link', 'drupalThemeGetSetting', 'addActiveClasses', 'addClassesForNonMenuPages'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $leftTier1Link1 = array('title' => 'link 1 title', 'url' => 'link_1_url', 'current_page_classes' => '');
    $leftTier1Link2 = array('title' => 'link 2 title', 'url' => 'link_2_url', 'current_page_classes' => '');
    $leftTier1Link3 = array('title' => 'link 3 title', 'url' => 'link_3_url', 'current_page_classes' => '');
    $leftTier1Link4 = array('title' => 'link 4 title', 'url' => 'link_4_url', 'current_page_classes' => '');
    $leftTier1Link5 = array('title' => 'link 5 title', 'url' => 'link_5_url', 'current_page_classes' => '');
    $leftTier1Link6 = array('title' => 'link 6 title', 'url' => 'link_6_url', 'current_page_classes' => '');
    $leftTier1Link7 = array('title' => 'link 7 title', 'url' => 'link_7_url', 'current_page_classes' => '');

    $getLeftTier1LinkCalls = [
      ['current', 'current alias', 1, $leftTier1Link1],
      ['current', 'current alias', 2, $leftTier1Link2],
      ['current', 'current alias', 3, $leftTier1Link3],
      ['current', 'current alias', 4, $leftTier1Link4],
      ['current', 'current alias', 5, $leftTier1Link5],
      ['current', 'current alias', 6, $leftTier1Link6],
      ['current', 'current alias', 7, $leftTier1Link7]
    ];

    $myMockHelper->expects($this->exactly(7))
                     ->method('getLeftTier1Link')
                     ->will($this->returnValueMap($getLeftTier1LinkCalls));

    $drupalThemeGetSettingCalls = [
      // Tier1 Link 1
      ['tier1link1tier2title1', 'tier1link1tier2link1-title'],
      ['tier1link1tier2uri1', 'tier1link1tier2link1-uri'],
      ['lefttier1title1', 'tier1link1tier2link1-tier1title'],
      ['tier1link1tier2title2', 'tier1link1tier2link2-title'],
      ['tier1link1tier2uri2', 'tier1link1tier2link2-uri'],
      ['lefttier1title2', 'tier1link1tier2link2-tier1title'],
      ['tier1link1tier2title3', 'tier1link1tier2link3-title'],
      ['tier1link1tier2uri3', 'tier1link1tier2link3-uri'],
      ['lefttier1title3', 'tier1link1tier2link3-tier1title'],
      ['tier1link1tier2title4', 'tier1link1tier2link4-title'],
      ['tier1link1tier2uri4', 'tier1link1tier2link4-uri'],
      ['lefttier1title4', 'tier1link1tier2link4-tier1title'],
      ['tier1link1tier2title5', 'tier1link1tier2link5-title'],
      ['tier1link1tier2uri5', 'tier1link1tier2link5-uri'],
      ['lefttier1title5', 'tier1link1tier2link5-tier1title'],
      // Tier1 Link2
      ['tier1link2tier2title1', 'tier1link2tier2link1-title'],
      ['tier1link2tier2uri1', 'tier1link2tier2link1-uri'],
      ['lefttier2title1', 'tier1link2tier2link1-tier1title'],
      ['tier1link2tier2title2', 'tier1link2tier2link2-title'],
      ['tier1link2tier2uri2', 'tier1link2tier2link2-uri'],
      ['lefttier2title2', 'tier1link2tier2link2-tier1title'],
      ['tier1link2tier2title3', 'tier1link2tier2link3-title'],
      ['tier1link2tier2uri3', 'tier1link2tier2link3-uri'],
      ['lefttier2title3', 'tier1link2tier2link3-tier1title'],
      ['tier1link2tier2title4', 'tier1link2tier2link4-title'],
      ['tier1link2tier2uri4', 'tier1link2tier2link4-uri'],
      ['lefttier2title4', 'tier1link2tier2link4-tier1title'],
      ['tier1link2tier2title5', 'tier1link2tier2link5-title'],
      ['tier1link2tier2uri5', 'tier1link2tier2link5-uri'],
      ['lefttier2title5', 'tier1link2tier2link5-tier1title'],
      // Tier1 Link3
      ['tier1link3tier2title1', 'tier1link3tier2link1-title'],
      ['tier1link3tier2uri1', 'tier1link3tier2link1-uri'],
      ['lefttier3title1', 'tier1link3tier2link1-tier1title'],
      ['tier1link3tier2title2', 'tier1link3tier2link2-title'],
      ['tier1link3tier2uri2', 'tier1link3tier2link2-uri'],
      ['lefttier3title2', 'tier1link3tier2link2-tier1title'],
      ['tier1link3tier2title3', 'tier1link3tier2link3-title'],
      ['tier1link3tier2uri3', 'tier1link3tier2link3-uri'],
      ['lefttier3title3', 'tier1link3tier2link3-tier1title'],
      ['tier1link3tier2title4', 'tier1link3tier2link4-title'],
      ['tier1link3tier2uri4', 'tier1link3tier2link4-uri'],
      ['lefttier3title4', 'tier1link3tier2link4-tier1title'],
      ['tier1link3tier2title5', 'tier1link3tier2link5-title'],
      ['tier1link3tier2uri5', 'tier1link3tier2link5-uri'],
      ['lefttier3title5', 'tier1link3tier2link5-tier1title'],
      // Tier1 Link4
      ['tier1link4tier2title1', 'tier1link4tier2link1-title'],
      ['tier1link4tier2uri1', 'tier1link4tier2link1-uri'],
      ['lefttier4title1', 'tier1link4tier2link1-tier1title'],
      ['tier1link4tier2title2', 'tier1link4tier2link2-title'],
      ['tier1link4tier2uri2', 'tier1link4tier2link2-uri'],
      ['lefttier4title2', 'tier1link4tier2link2-tier1title'],
      ['tier1link4tier2title3', 'tier1link4tier2link3-title'],
      ['tier1link4tier2uri3', 'tier1link4tier2link3-uri'],
      ['lefttier4title3', 'tier1link4tier2link3-tier1title'],
      ['tier1link4tier2title4', 'tier1link4tier2link4-title'],
      ['tier1link4tier2uri4', 'tier1link4tier2link4-uri'],
      ['lefttier4title4', 'tier1link4tier2link4-tier1title'],
      ['tier1link4tier2title5', 'tier1link4tier2link5-title'],
      ['tier1link4tier2uri5', 'tier1link4tier2link5-uri'],
      ['lefttier4title5', 'tier1link4tier2link5-tier1title'],
      // Tier1 Link5
      ['tier1link5tier2title1', 'tier1link5tier2link1-title'],
      ['tier1link5tier2uri1', 'tier1link5tier2link1-uri'],
      ['lefttier5title1', 'tier1link5tier2link1-tier1title'],
      ['tier1link5tier2title2', 'tier1link5tier2link2-title'],
      ['tier1link5tier2uri2', 'tier1link5tier2link2-uri'],
      ['lefttier5title2', 'tier1link5tier2link2-tier1title'],
      ['tier1link5tier2title3', 'tier1link5tier2link3-title'],
      ['tier1link5tier2uri3', 'tier1link5tier2link3-uri'],
      ['lefttier5title3', 'tier1link5tier2link3-tier1title'],
      ['tier1link5tier2title4', 'tier1link5tier2link4-title'],
      ['tier1link5tier2uri4', 'tier1link5tier2link4-uri'],
      ['lefttier5title4', 'tier1link5tier2link4-tier1title'],
      ['tier1link5tier2title5', 'tier1link5tier2link5-title'],
      ['tier1link5tier2uri5', 'tier1link5tier2link5-uri'],
      ['lefttier5title5', 'tier1link5tier2link5-tier1title'],
      // Tier1 Link6
      ['tier1link6tier2title1', 'tier1link6tier2link1-title'],
      ['tier1link6tier2uri1', 'tier1link6tier2link1-uri'],
      ['lefttier6title1', 'tier1link6tier2link1-tier1title'],
      ['tier1link6tier2title2', 'tier1link6tier2link2-title'],
      ['tier1link6tier2uri2', 'tier1link6tier2link2-uri'],
      ['lefttier6title2', 'tier1link6tier2link2-tier1title'],
      ['tier1link6tier2title3', 'tier1link6tier2link3-title'],
      ['tier1link6tier2uri3', 'tier1link6tier2link3-uri'],
      ['lefttier6title3', 'tier1link6tier2link3-tier1title'],
      ['tier1link6tier2title4', 'tier1link6tier2link4-title'],
      ['tier1link6tier2uri4', 'tier1link6tier2link4-uri'],
      ['lefttier6title4', 'tier1link6tier2link4-tier1title'],
      ['tier1link6tier2title5', 'tier1link6tier2link5-title'],
      ['tier1link6tier2uri5', 'tier1link6tier2link5-uri'],
      ['lefttier6title5', 'tier1link6tier2link5-tier1title'],
      // Tier1 Link7
      ['tier1link7tier2title1', 'tier1link7tier2link1-title'],
      ['tier1link7tier2uri1', 'tier1link7tier2link1-uri'],
      ['lefttier7title1', 'tier1link7tier2link1-tier1title'],
      ['tier1link7tier2title2', 'tier1link7tier2link2-title'],
      ['tier1link7tier2uri2', 'tier1link7tier2link2-uri'],
      ['lefttier7title2', 'tier1link7tier2link2-tier1title'],
      ['tier1link7tier2title3', 'tier1link7tier2link3-title'],
      ['tier1link7tier2uri3', 'tier1link7tier2link3-uri'],
      ['lefttier7title3', 'tier1link7tier2link3-tier1title'],
      ['tier1link7tier2title4', 'tier1link7tier2link4-title'],
      ['tier1link7tier2uri4', 'tier1link7tier2link4-uri'],
      ['lefttier7title4', 'tier1link7tier2link4-tier1title'],
      ['tier1link7tier2title5', 'tier1link7tier2link5-title'],
      ['tier1link7tier2uri5', 'tier1link7tier2link5-uri'],
      ['lefttier7title5', 'tier1link7tier2link5-tier1title']
    ];
    // You read that right...105
    $myMockHelper->expects($this->exactly(105))
                 ->method('drupalThemeGetSetting')
                 ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $mockedActiveClassReturn = array();
    $mockedActiveClassReturn['tier1'] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $mockedActiveClassReturn['tier2'] = array('title' => 'tier2 link', 'url' => 'tier2 url');

    $myMockHelper->expects($this->exactly(42))
                 ->method('addActiveClasses')
                 ->will($this->returnValue($mockedActiveClassReturn));

    $expectedResult = array();
    $tier1HeaderLinks = array();
    $tier1HeaderLinks[0] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier1HeaderLinks[1] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier1HeaderLinks[2] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier1HeaderLinks[3] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier1HeaderLinks[4] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier1HeaderLinks[5] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier1HeaderLinks[6] = array('title' => 'tier1 link', 'url' => 'tier1 url');
    $tier2HeaderLinks = array();
    $tier2HeaderLinks[0] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[1] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[2] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[3] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[4] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[5] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[6] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[7] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[8] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[9] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[10] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[11] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[12] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[13] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[14] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[15] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[16] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[17] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[18] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[19] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[20] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[21] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[22] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[23] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[24] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[25] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[26] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[27] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[28] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[29] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[30] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[31] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[32] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[33] = array('title' => 'tier2 link', 'url' => 'tier2 url');
    $tier2HeaderLinks[34] = array('title' => 'tier2 link', 'url' => 'tier2 url');

    $myMockHelper->expects($this->exactly(1))
                 ->method('addClassesForNonMenuPages')
                 ->will($this->returnValue($tier1HeaderLinks));

    $expectedResult['tier1'] = $tier1HeaderLinks;
    $expectedResult['tier2'] = $tier2HeaderLinks;

    $result = $myMockHelper->getHeaderMenuLinks($currentPath, $currentPathAlias);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetHeaderMenuLinks_titleAndUrlNotSet() {
    $currentPath = 'current';
    $currentPathAlias = 'current alias';

    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('getLeftTier1Link', 'drupalThemeGetSetting', 'addActiveClasses', 'addClassesForNonMenuPages'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $leftTier1Link1 = array('current_page_classes' => '');
    $leftTier1Link2 = array('current_page_classes' => '');
    $leftTier1Link3 = array('current_page_classes' => '');
    $leftTier1Link4 = array('current_page_classes' => '');
    $leftTier1Link5 = array('current_page_classes' => '');
    $leftTier1Link6 = array('current_page_classes' => '');
    $leftTier1Link7 = array('current_page_classes' => '');

    $getLeftTier1LinkCalls = [
      ['current', 'current alias', 1, $leftTier1Link1],
      ['current', 'current alias', 2, $leftTier1Link2],
      ['current', 'current alias', 3, $leftTier1Link3],
      ['current', 'current alias', 4, $leftTier1Link4],
      ['current', 'current alias', 5, $leftTier1Link5],
      ['current', 'current alias', 6, $leftTier1Link6],
      ['current', 'current alias', 7, $leftTier1Link7]
    ];

    $myMockHelper->expects($this->exactly(7))
                     ->method('getLeftTier1Link')
                     ->will($this->returnValueMap($getLeftTier1LinkCalls));

    $drupalThemeGetSettingCalls = [
      // Tier1 Link 1
      ['tier1link1tier2title1', ''],
      ['tier1link1tier2uri1', ''],
      ['lefttier1title1', 'tier1link1tier2link1-tier1title'],
      ['tier1link1tier2title2', ''],
      ['tier1link1tier2uri2', ''],
      ['lefttier1title2', 'tier1link1tier2link2-tier1title'],
      ['tier1link1tier2title3', ''],
      ['tier1link1tier2uri3', ''],
      ['lefttier1title3', 'tier1link1tier2link3-tier1title'],
      ['tier1link1tier2title4', ''],
      ['tier1link1tier2uri4', ''],
      ['lefttier1title4', 'tier1link1tier2link4-tier1title'],
      ['tier1link1tier2title5', ''],
      ['tier1link1tier2uri5', ''],
      ['lefttier1title5', 'tier1link1tier2link5-tier1title'],
      // Tier1 Link2
      ['tier1link2tier2title1', ''],
      ['tier1link2tier2uri1', ''],
      ['lefttier2title1', 'tier1link2tier2link1-tier1title'],
      ['tier1link2tier2title2', ''],
      ['tier1link2tier2uri2', ''],
      ['lefttier2title2', 'tier1link2tier2link2-tier1title'],
      ['tier1link2tier2title3', ''],
      ['tier1link2tier2uri3', ''],
      ['lefttier2title3', 'tier1link2tier2link3-tier1title'],
      ['tier1link2tier2title4', ''],
      ['tier1link2tier2uri4', ''],
      ['lefttier2title4', 'tier1link2tier2link4-tier1title'],
      ['tier1link2tier2title5', ''],
      ['tier1link2tier2uri5', ''],
      ['lefttier2title5', 'tier1link2tier2link5-tier1title'],
      // Tier1 Link3
      ['tier1link3tier2title1', ''],
      ['tier1link3tier2uri1', ''],
      ['lefttier3title1', 'tier1link3tier2link1-tier1title'],
      ['tier1link3tier2title2', ''],
      ['tier1link3tier2uri2', ''],
      ['lefttier3title2', 'tier1link3tier2link2-tier1title'],
      ['tier1link3tier2title3', ''],
      ['tier1link3tier2uri3', ''],
      ['lefttier3title3', 'tier1link3tier2link3-tier1title'],
      ['tier1link3tier2title4', ''],
      ['tier1link3tier2uri4', ''],
      ['lefttier3title4', 'tier1link3tier2link4-tier1title'],
      ['tier1link3tier2title5', ''],
      ['tier1link3tier2uri5', ''],
      ['lefttier3title5', 'tier1link3tier2link5-tier1title'],
      // Tier1 Link4
      ['tier1link4tier2title1', ''],
      ['tier1link4tier2uri1', ''],
      ['lefttier4title1', 'tier1link4tier2link1-tier1title'],
      ['tier1link4tier2title2', ''],
      ['tier1link4tier2uri2', ''],
      ['lefttier4title2', 'tier1link4tier2link2-tier1title'],
      ['tier1link4tier2title3', ''],
      ['tier1link4tier2uri3', ''],
      ['lefttier4title3', 'tier1link4tier2link3-tier1title'],
      ['tier1link4tier2title4', ''],
      ['tier1link4tier2uri4', ''],
      ['lefttier4title4', 'tier1link4tier2link4-tier1title'],
      ['tier1link4tier2title5', ''],
      ['tier1link4tier2uri5', ''],
      ['lefttier4title5', 'tier1link4tier2link5-tier1title'],
      // Tier1 Link5
      ['tier1link5tier2title1', ''],
      ['tier1link5tier2uri1', ''],
      ['lefttier5title1', 'tier1link5tier2link1-tier1title'],
      ['tier1link5tier2title2', ''],
      ['tier1link5tier2uri2', ''],
      ['lefttier5title2', 'tier1link5tier2link2-tier1title'],
      ['tier1link5tier2title3', ''],
      ['tier1link5tier2uri3', ''],
      ['lefttier5title3', 'tier1link5tier2link3-tier1title'],
      ['tier1link5tier2title4', ''],
      ['tier1link5tier2uri4', ''],
      ['lefttier5title4', 'tier1link5tier2link4-tier1title'],
      ['tier1link5tier2title5', ''],
      ['tier1link5tier2uri5', ''],
      ['lefttier5title5', 'tier1link5tier2link5-tier1title'],
      // Tier1 Link6
      ['tier1link6tier2title1', ''],
      ['tier1link6tier2uri1', ''],
      ['lefttier6title1', 'tier1link6tier2link1-tier1title'],
      ['tier1link6tier2title2', ''],
      ['tier1link6tier2uri2', ''],
      ['lefttier6title2', 'tier1link6tier2link2-tier1title'],
      ['tier1link6tier2title3', ''],
      ['tier1link6tier2uri3', ''],
      ['lefttier6title3', 'tier1link6tier2link3-tier1title'],
      ['tier1link6tier2title4', ''],
      ['tier1link6tier2uri4', ''],
      ['lefttier6title4', 'tier1link6tier2link4-tier1title'],
      ['tier1link6tier2title5', ''],
      ['tier1link6tier2uri5', ''],
      ['lefttier6title5', 'tier1link6tier2link5-tier1title'],
      // Tier1 Link7
      ['tier1link7tier2title1', ''],
      ['tier1link7tier2uri1', ''],
      ['lefttier7title1', 'tier1link7tier2link1-tier1title'],
      ['tier1link7tier2title2', ''],
      ['tier1link7tier2uri2', ''],
      ['lefttier7title2', 'tier1link7tier2link2-tier1title'],
      ['tier1link7tier2title3', ''],
      ['tier1link7tier2uri3', ''],
      ['lefttier7title3', 'tier1link7tier2link3-tier1title'],
      ['tier1link7tier2title4', ''],
      ['tier1link7tier2uri4', ''],
      ['lefttier7title4', 'tier1link7tier2link4-tier1title'],
      ['tier1link7tier2title5', ''],
      ['tier1link7tier2uri5', ''],
      ['lefttier7title5', 'tier1link7tier2link5-tier1title']
    ];
    // You read that right...105
    $myMockHelper->expects($this->exactly(105))
                 ->method('drupalThemeGetSetting')
                 ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $myMockHelper->expects($this->exactly(1))
                 ->method('addClassesForNonMenuPages')
                 ->will($this->returnValue(array()));

    $myMockHelper->expects($this->never())
                 ->method('addActiveClasses');

    $expectedResult = array();
    $expectedResult['tier1'] = array();
    $expectedResult['tier2'] = array();

    $result = $myMockHelper->getHeaderMenuLinks($currentPath, $currentPathAlias);
    $this->assertSame($expectedResult, $result);
  }

  public function testBuildFooterThemeSettings_happy() {
    $form = array();
    $defaultLegalFooterContent = 'default legal footer content';
    $defaultFooterLinks = array();
    $defaultFooterLinks[1] = array('title' => 'footer link 1', 'url' => 'link1_url');
    $defaultFooterLinks[2] = array('title' => 'footer link 2', 'url' => 'link2_url');
    $defaultFooterLinks[3] = array('title' => 'footer link 3', 'url' => 'link3_url');
    $defaultFooterLinks[4] = array('title' => 'footer link 4', 'url' => 'link4_url');
    $defaultFooterLinks[5] = array('title' => 'footer link 5', 'url' => 'link5_url');
    $defaultFooterLinks[6] = array('title' => 'footer link 6', 'url' => 'link6_url');
    $defaultFooterLinks[7] = array('title' => 'footer link 7', 'url' => 'link7_url');

    // TODO: check with Elle that these mocks are correct
    $this->mockHelper->expects($this->once())
                     ->method('getDefaultLegalFooterContent')
                     ->will($this->returnValue($defaultLegalFooterContent));

    $this->mockHelper->expects($this->once())
                     ->method('getDefaultFooterLinks')
                     ->will($this->returnValue($defaultFooterLinks));

    $drupalTCalls = [
      ['Include Social Icons?', 'Drupal Teed: Include Social Icons?'],
      ['Footer Links', 'Drupal Teed: Footer Links'],
      ['Footer Link 1', 'Drupal Teed: Footer Link 1'],
      ['Footer Link 2', 'Drupal Teed: Footer Link 2'],
      ['Footer Link 3', 'Drupal Teed: Footer Link 3'],
      ['Footer Link 4', 'Drupal Teed: Footer Link 4'],
      ['Footer Link 5', 'Drupal Teed: Footer Link 5'],
      ['Footer Link 6', 'Drupal Teed: Footer Link 6'],
      ['Footer Link 7', 'Drupal Teed: Footer Link 7'],
      ['Text for link 1', 'Drupal Teed: Text for link 1'],
      ['Text for link 2', 'Drupal Teed: Text for link 2'],
      ['Text for link 3', 'Drupal Teed: Text for link 3'],
      ['Text for link 4', 'Drupal Teed: Text for link 4'],
      ['Text for link 5', 'Drupal Teed: Text for link 5'],
      ['Text for link 6', 'Drupal Teed: Text for link 6'],
      ['Text for link 7', 'Drupal Teed: Text for link 7'],
      ['URL for link 1', 'Drupal Teed: URL for link 1'],
      ['URL for link 2', 'Drupal Teed: URL for link 2'],
      ['URL for link 3', 'Drupal Teed: URL for link 3'],
      ['URL for link 4', 'Drupal Teed: URL for link 4'],
      ['URL for link 5', 'Drupal Teed: URL for link 5'],
      ['URL for link 6', 'Drupal Teed: URL for link 6'],
      ['URL for link 7', 'Drupal Teed: URL for link 7'],
      ['Legal Footer Content', 'Drupal Teed: Legal Footer Content']
    ];

    $this->mockHelper->expects($this->exactly(24))
                     ->method('drupalT')
                     ->will($this->returnValueMap($drupalTCalls));

    $drupalThemeGetSettingCalls = [
      ['includesocial', 'includesocial setting'],
      ['footertitle1', 'footertitle1 setting'],
      ['footertitle2', 'footertitle2 setting'],
      ['footertitle3', 'footertitle3 setting'],
      ['footertitle4', 'footertitle4 setting'],
      ['footertitle5', 'footertitle5 setting'],
      ['footertitle6', 'footertitle6 setting'],
      ['footertitle7', 'footertitle7 setting'],
      ['footeruri1', 'footeruri1 setting'],
      ['footeruri2', 'footeruri2 setting'],
      ['footeruri3', 'footeruri3 setting'],
      ['footeruri4', 'footeruri4 setting'],
      ['footeruri5', 'footeruri5 setting'],
      ['footeruri6', 'footeruri6 setting'],
      ['footeruri7', 'footeruri7 setting'],
      ['legalfooter', 'legalfooter setting']
    ];

    $this->mockHelper->expects($this->exactly(31))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();
    $expectedResult['includesocial'] = array(
      '#type'  => 'checkbox',
      '#title' => 'Drupal Teed: Include Social Icons?',
      '#group' => 'footer',
      '#default_value' => 'includesocial setting',
      '#weight' => -10
    );
    $expectedResult['footerlinks'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Links',
      '#open' => false,
      '#group' => 'footer',
      '#weight' => 0
    );
    $expectedResult['footerlink1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 1',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'footertitle1 setting',
      '#group' => 'footerlink1'
    );
    $expectedResult['footeruri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'footeruri1 setting',
      '#group' => 'footerlink1'
    );
    $expectedResult['footerlink2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 2',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'footertitle2 setting',
      '#group' => 'footerlink2'
    );
    $expectedResult['footeruri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'footeruri2 setting',
      '#group' => 'footerlink2'
    );
    $expectedResult['footerlink3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 3',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'footertitle3 setting',
      '#group' => 'footerlink3'
    );
    $expectedResult['footeruri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'footeruri3 setting',
      '#group' => 'footerlink3'
    );
    $expectedResult['footerlink4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 4',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'footertitle4 setting',
      '#group' => 'footerlink4'
    );
    $expectedResult['footeruri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'footeruri4 setting',
      '#group' => 'footerlink4'
    );
    $expectedResult['footerlink5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 5',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'footertitle5 setting',
      '#group' => 'footerlink5'
    );
    $expectedResult['footeruri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'footeruri5 setting',
      '#group' => 'footerlink5'
    );
    $expectedResult['footerlink6'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 6',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle6'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 6',
      '#default_value' => 'footertitle6 setting',
      '#group' => 'footerlink6'
    );
    $expectedResult['footeruri6'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 6',
      '#default_value' => 'footeruri6 setting',
      '#group' => 'footerlink6'
    );
    $expectedResult['footerlink7'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 7',
      '#open' => false,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle7'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 7',
      '#default_value' => 'footertitle7 setting',
      '#group' => 'footerlink7'
    );
    $expectedResult['footeruri7'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 7',
      '#default_value' => 'footeruri7 setting',
      '#group' => 'footerlink7'
    );
    $expectedResult['legalfooter'] = array(
      '#type'  => 'textarea',
      '#title' => 'Drupal Teed: Legal Footer Content',
      '#default_value' => 'legalfooter setting',
      '#group' => 'footer',
      '#weight' => 10
    );

    $result = $this->mockHelper->buildFooterThemeSettings($form);
    $this->assertSame($expectedResult, $result);
  }

  public function testBuildFooterThemeSettings_themeSettingsEmpty_useDefaultsInstead() {
    $form = array();
    $defaultLegalFooterContent = 'default legal footer content';
    $defaultFooterLinks = array();
    $defaultFooterLinks[1] = array('title' => 'footer link 1', 'url' => 'link1_url');
    $defaultFooterLinks[2] = array('title' => 'footer link 2', 'url' => 'link2_url');
    $defaultFooterLinks[3] = array('title' => 'footer link 3', 'url' => 'link3_url');
    $defaultFooterLinks[4] = array('title' => 'footer link 4', 'url' => 'link4_url');
    $defaultFooterLinks[5] = array('title' => 'footer link 5', 'url' => 'link5_url');
    $defaultFooterLinks[6] = array('title' => 'footer link 6', 'url' => 'link6_url');
    $defaultFooterLinks[7] = array('title' => 'footer link 7', 'url' => 'link7_url');

    // TODO: check with Elle that these mocks are correct
    $this->mockHelper->expects($this->once())
                     ->method('getDefaultLegalFooterContent')
                     ->will($this->returnValue($defaultLegalFooterContent));

    $this->mockHelper->expects($this->once())
                     ->method('getDefaultFooterLinks')
                     ->will($this->returnValue($defaultFooterLinks));

    $drupalTCalls = [
      ['Include Social Icons?', 'Drupal Teed: Include Social Icons?'],
      ['Footer Links', 'Drupal Teed: Footer Links'],
      ['Footer Link 1', 'Drupal Teed: Footer Link 1'],
      ['Footer Link 2', 'Drupal Teed: Footer Link 2'],
      ['Footer Link 3', 'Drupal Teed: Footer Link 3'],
      ['Footer Link 4', 'Drupal Teed: Footer Link 4'],
      ['Footer Link 5', 'Drupal Teed: Footer Link 5'],
      ['Footer Link 6', 'Drupal Teed: Footer Link 6'],
      ['Footer Link 7', 'Drupal Teed: Footer Link 7'],
      ['Text for link 1', 'Drupal Teed: Text for link 1'],
      ['Text for link 2', 'Drupal Teed: Text for link 2'],
      ['Text for link 3', 'Drupal Teed: Text for link 3'],
      ['Text for link 4', 'Drupal Teed: Text for link 4'],
      ['Text for link 5', 'Drupal Teed: Text for link 5'],
      ['Text for link 6', 'Drupal Teed: Text for link 6'],
      ['Text for link 7', 'Drupal Teed: Text for link 7'],
      ['URL for link 1', 'Drupal Teed: URL for link 1'],
      ['URL for link 2', 'Drupal Teed: URL for link 2'],
      ['URL for link 3', 'Drupal Teed: URL for link 3'],
      ['URL for link 4', 'Drupal Teed: URL for link 4'],
      ['URL for link 5', 'Drupal Teed: URL for link 5'],
      ['URL for link 6', 'Drupal Teed: URL for link 6'],
      ['URL for link 7', 'Drupal Teed: URL for link 7'],
      ['Legal Footer Content', 'Drupal Teed: Legal Footer Content']
    ];

    $this->mockHelper->expects($this->exactly(24))
                     ->method('drupalT')
                     ->will($this->returnValueMap($drupalTCalls));

    $this->mockHelper->expects($this->exactly(16))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue(''));

    $expectedResult = array();
    $expectedResult['includesocial'] = array(
      '#type'  => 'checkbox',
      '#title' => 'Drupal Teed: Include Social Icons?',
      '#group' => 'footer',
      '#default_value' => '',
      '#weight' => -10
    );
    $expectedResult['footerlinks'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Links',
      '#open' => false,
      '#group' => 'footer',
      '#weight' => 0
    );
    $expectedResult['footerlink1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 1',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'footer link 1',
      '#group' => 'footerlink1'
    );
    $expectedResult['footeruri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'link1_url',
      '#group' => 'footerlink1'
    );
    $expectedResult['footerlink2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 2',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'footer link 2',
      '#group' => 'footerlink2'
    );
    $expectedResult['footeruri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'link2_url',
      '#group' => 'footerlink2'
    );
    $expectedResult['footerlink3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 3',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'footer link 3',
      '#group' => 'footerlink3'
    );
    $expectedResult['footeruri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'link3_url',
      '#group' => 'footerlink3'
    );
    $expectedResult['footerlink4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 4',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'footer link 4',
      '#group' => 'footerlink4'
    );
    $expectedResult['footeruri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'link4_url',
      '#group' => 'footerlink4'
    );
    $expectedResult['footerlink5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 5',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'footer link 5',
      '#group' => 'footerlink5'
    );
    $expectedResult['footeruri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'link5_url',
      '#group' => 'footerlink5'
    );
    $expectedResult['footerlink6'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 6',
      '#open' => true,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle6'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 6',
      '#default_value' => 'footer link 6',
      '#group' => 'footerlink6'
    );
    $expectedResult['footeruri6'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 6',
      '#default_value' => 'link6_url',
      '#group' => 'footerlink6'
    );
    $expectedResult['footerlink7'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Footer Link 7',
      '#open' => false,
      '#group' => 'footerlinks'
    );
    $expectedResult['footertitle7'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 7',
      '#default_value' => 'footer link 7',
      '#group' => 'footerlink7'
    );
    $expectedResult['footeruri7'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 7',
      '#default_value' => 'link7_url',
      '#group' => 'footerlink7'
    );
    $expectedResult['legalfooter'] = array(
      '#type'  => 'textarea',
      '#title' => 'Drupal Teed: Legal Footer Content',
      '#default_value' => 'default legal footer content',
      '#group' => 'footer',
      '#weight' => 10
    );

    $result = $this->mockHelper->buildFooterThemeSettings($form);
    $this->assertSame($expectedResult, $result);
  }

  public function testBuildHeaderThemeSettings() {
    $form = array();

    $drupalTCalls = [
      ['Tier1 Header Links - Left Aligned', 'Drupal Teed: Tier1 Header Links - Left Aligned'],
      ['Tier1 Header Links - Right Aligned', 'Drupal Teed: Tier1 Header Links - Right Aligned'],
      ['Left Tier1 Header Link 1', 'Drupal Teed: Left Tier1 Header Link 1'],
      ['Left Tier1 Header Link 2', 'Drupal Teed: Left Tier1 Header Link 2'],
      ['Left Tier1 Header Link 3', 'Drupal Teed: Left Tier1 Header Link 3'],
      ['Left Tier1 Header Link 4', 'Drupal Teed: Left Tier1 Header Link 4'],
      ['Left Tier1 Header Link 5', 'Drupal Teed: Left Tier1 Header Link 5'],
      ['Left Tier1 Header Link 6', 'Drupal Teed: Left Tier1 Header Link 6'],
      ['Left Tier1 Header Link 7', 'Drupal Teed: Left Tier1 Header Link 7'],
      ['Text for link 1', 'Drupal Teed: Text for link 1'],
      ['Text for link 2', 'Drupal Teed: Text for link 2'],
      ['Text for link 3', 'Drupal Teed: Text for link 3'],
      ['Text for link 4', 'Drupal Teed: Text for link 4'],
      ['Text for link 5', 'Drupal Teed: Text for link 5'],
      ['Text for link 6', 'Drupal Teed: Text for link 6'],
      ['Text for link 7', 'Drupal Teed: Text for link 7'],
      ['URL for link 1', 'Drupal Teed: URL for link 1'],
      ['URL for link 2', 'Drupal Teed: URL for link 2'],
      ['URL for link 3', 'Drupal Teed: URL for link 3'],
      ['URL for link 4', 'Drupal Teed: URL for link 4'],
      ['URL for link 5', 'Drupal Teed: URL for link 5'],
      ['URL for link 6', 'Drupal Teed: URL for link 6'],
      ['URL for link 7', 'Drupal Teed: URL for link 7'],
      ['Tier2 Header Link 1', 'Drupal Teed: Tier2 Header Link 1'],
      ['Tier2 Header Link 2', 'Drupal Teed: Tier2 Header Link 2'],
      ['Tier2 Header Link 3', 'Drupal Teed: Tier2 Header Link 3'],
      ['Tier2 Header Link 4', 'Drupal Teed: Tier2 Header Link 4'],
      ['Tier2 Header Link 5', 'Drupal Teed: Tier2 Header Link 5'],
      ['Right Tier1 Header Link 1', 'Drupal Teed: Right Tier1 Header Link 1'],
      ['Right Tier1 Header Link 2', 'Drupal Teed: Right Tier1 Header Link 2']
    ];

    $this->mockHelper->expects($this->exactly(134))
                     ->method('drupalT')
                     ->will($this->returnValueMap($drupalTCalls));

    $drupalThemeGetSettingCalls = [
      ['lefttier1title1', 'Setting: lefttier1title1'],
      ['lefttier1title2', 'Setting: lefttier1title2'],
      ['lefttier1title3', 'Setting: lefttier1title3'],
      ['lefttier1title4', 'Setting: lefttier1title4'],
      ['lefttier1title5', 'Setting: lefttier1title5'],
      ['lefttier1title6', 'Setting: lefttier1title6'],
      ['lefttier1title7', 'Setting: lefttier1title7'],
      ['lefttier1uri1', 'Setting: lefttier1uri1'],
      ['lefttier1uri2', 'Setting: lefttier1uri2'],
      ['lefttier1uri3', 'Setting: lefttier1uri3'],
      ['lefttier1uri4', 'Setting: lefttier1uri4'],
      ['lefttier1uri5', 'Setting: lefttier1uri5'],
      ['lefttier1uri6', 'Setting: lefttier1uri6'],
      ['lefttier1uri7', 'Setting: lefttier1uri7'],
      ['tier1link1tier2title1', 'Setting: tier1link1tier2title1'],
      ['tier1link1tier2title2', 'Setting: tier1link1tier2title2'],
      ['tier1link1tier2title3', 'Setting: tier1link1tier2title3'],
      ['tier1link1tier2title4', 'Setting: tier1link1tier2title4'],
      ['tier1link1tier2title5', 'Setting: tier1link1tier2title5'],
      ['tier1link2tier2title1', 'Setting: tier1link2tier2title1'],
      ['tier1link2tier2title2', 'Setting: tier1link2tier2title2'],
      ['tier1link2tier2title3', 'Setting: tier1link2tier2title3'],
      ['tier1link2tier2title4', 'Setting: tier1link2tier2title4'],
      ['tier1link2tier2title5', 'Setting: tier1link2tier2title5'],
      ['tier1link3tier2title1', 'Setting: tier1link3tier2title1'],
      ['tier1link3tier2title2', 'Setting: tier1link3tier2title2'],
      ['tier1link3tier2title3', 'Setting: tier1link3tier2title3'],
      ['tier1link3tier2title4', 'Setting: tier1link3tier2title4'],
      ['tier1link3tier2title5', 'Setting: tier1link3tier2title5'],
      ['tier1link4tier2title1', 'Setting: tier1link4tier2title1'],
      ['tier1link4tier2title2', 'Setting: tier1link4tier2title2'],
      ['tier1link4tier2title3', 'Setting: tier1link4tier2title3'],
      ['tier1link4tier2title4', 'Setting: tier1link4tier2title4'],
      ['tier1link4tier2title5', 'Setting: tier1link4tier2title5'],
      ['tier1link5tier2title1', 'Setting: tier1link5tier2title1'],
      ['tier1link5tier2title2', 'Setting: tier1link5tier2title2'],
      ['tier1link5tier2title3', 'Setting: tier1link5tier2title3'],
      ['tier1link5tier2title4', 'Setting: tier1link5tier2title4'],
      ['tier1link5tier2title5', 'Setting: tier1link5tier2title5'],
      ['tier1link6tier2title1', 'Setting: tier1link6tier2title1'],
      ['tier1link6tier2title2', 'Setting: tier1link6tier2title2'],
      ['tier1link6tier2title3', 'Setting: tier1link6tier2title3'],
      ['tier1link6tier2title4', 'Setting: tier1link6tier2title4'],
      ['tier1link6tier2title5', 'Setting: tier1link6tier2title5'],
      ['tier1link7tier2title1', 'Setting: tier1link7tier2title1'],
      ['tier1link7tier2title2', 'Setting: tier1link7tier2title2'],
      ['tier1link7tier2title3', 'Setting: tier1link7tier2title3'],
      ['tier1link7tier2title4', 'Setting: tier1link7tier2title4'],
      ['tier1link7tier2title5', 'Setting: tier1link7tier2title5'],
      ['tier1link1tier2uri1', 'Setting: tier1link1tier2uri1'],
      ['tier1link1tier2uri2', 'Setting: tier1link1tier2uri2'],
      ['tier1link1tier2uri3', 'Setting: tier1link1tier2uri3'],
      ['tier1link1tier2uri4', 'Setting: tier1link1tier2uri4'],
      ['tier1link1tier2uri5', 'Setting: tier1link1tier2uri5'],
      ['tier1link2tier2uri1', 'Setting: tier1link2tier2uri1'],
      ['tier1link2tier2uri2', 'Setting: tier1link2tier2uri2'],
      ['tier1link2tier2uri3', 'Setting: tier1link2tier2uri3'],
      ['tier1link2tier2uri4', 'Setting: tier1link2tier2uri4'],
      ['tier1link2tier2uri5', 'Setting: tier1link2tier2uri5'],
      ['tier1link3tier2uri1', 'Setting: tier1link3tier2uri1'],
      ['tier1link3tier2uri2', 'Setting: tier1link3tier2uri2'],
      ['tier1link3tier2uri3', 'Setting: tier1link3tier2uri3'],
      ['tier1link3tier2uri4', 'Setting: tier1link3tier2uri4'],
      ['tier1link3tier2uri5', 'Setting: tier1link3tier2uri5'],
      ['tier1link4tier2uri1', 'Setting: tier1link4tier2uri1'],
      ['tier1link4tier2uri2', 'Setting: tier1link4tier2uri2'],
      ['tier1link4tier2uri3', 'Setting: tier1link4tier2uri3'],
      ['tier1link4tier2uri4', 'Setting: tier1link4tier2uri4'],
      ['tier1link4tier2uri5', 'Setting: tier1link4tier2uri5'],
      ['tier1link5tier2uri1', 'Setting: tier1link5tier2uri1'],
      ['tier1link5tier2uri2', 'Setting: tier1link5tier2uri2'],
      ['tier1link5tier2uri3', 'Setting: tier1link5tier2uri3'],
      ['tier1link5tier2uri4', 'Setting: tier1link5tier2uri4'],
      ['tier1link5tier2uri5', 'Setting: tier1link5tier2uri5'],
      ['tier1link6tier2uri1', 'Setting: tier1link6tier2uri1'],
      ['tier1link6tier2uri2', 'Setting: tier1link6tier2uri2'],
      ['tier1link6tier2uri3', 'Setting: tier1link6tier2uri3'],
      ['tier1link6tier2uri4', 'Setting: tier1link6tier2uri4'],
      ['tier1link6tier2uri5', 'Setting: tier1link6tier2uri5'],
      ['tier1link7tier2uri1', 'Setting: tier1link7tier2uri1'],
      ['tier1link7tier2uri2', 'Setting: tier1link7tier2uri2'],
      ['tier1link7tier2uri3', 'Setting: tier1link7tier2uri3'],
      ['tier1link7tier2uri4', 'Setting: tier1link7tier2uri4'],
      ['tier1link7tier2uri5', 'Setting: tier1link7tier2uri5'],
      ['righttier1title1', 'Setting: righttier1title1'],
      ['righttier1title2', 'Setting: righttier1title2'],
      ['righttier1uri1', 'Setting: righttier1uri1'],
      ['righttier1uri2', 'Setting: righttier1uri2']
    ];

    $this->mockHelper->expects($this->exactly(88))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();
    $expectedResult['leftlinks'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier1 Header Links - Left Aligned',
      '#open' => true,
      '#group' => 'header'
    );
    $expectedResult['rightlinks'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier1 Header Links - Right Aligned',
      '#open' => true,
      '#group' => 'header'
    );
    $expectedResult['lefttier1link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 1',
      '#open' => true,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: lefttier1title1',
      '#group' => 'lefttier1link1'
    );
    $expectedResult['lefttier1uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: lefttier1uri1',
      '#group' => 'lefttier1link1'
    );
    $expectedResult['lefttier1link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 2',
      '#open' => true,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: lefttier1title2',
      '#group' => 'lefttier1link2'
    );
    $expectedResult['lefttier1uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: lefttier1uri2',
      '#group' => 'lefttier1link2'
    );
    $expectedResult['lefttier1link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 3',
      '#open' => true,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: lefttier1title3',
      '#group' => 'lefttier1link3'
    );
    $expectedResult['lefttier1uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: lefttier1uri3',
      '#group' => 'lefttier1link3'
    );
    $expectedResult['lefttier1link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 4',
      '#open' => false,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: lefttier1title4',
      '#group' => 'lefttier1link4'
    );
    $expectedResult['lefttier1uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: lefttier1uri4',
      '#group' => 'lefttier1link4'
    );
    $expectedResult['lefttier1link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 5',
      '#open' => false,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: lefttier1title5',
      '#group' => 'lefttier1link5'
    );
    $expectedResult['lefttier1uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: lefttier1uri5',
      '#group' => 'lefttier1link5'
    );
    $expectedResult['lefttier1link6'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 6',
      '#open' => false,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title6'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 6',
      '#default_value' => 'Setting: lefttier1title6',
      '#group' => 'lefttier1link6'
    );
    $expectedResult['lefttier1uri6'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 6',
      '#default_value' => 'Setting: lefttier1uri6',
      '#group' => 'lefttier1link6'
    );
    $expectedResult['lefttier1link7'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Left Tier1 Header Link 7',
      '#open' => false,
      '#group' => 'leftlinks'
    );
    $expectedResult['lefttier1title7'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 7',
      '#default_value' => 'Setting: lefttier1title7',
      '#group' => 'lefttier1link7'
    );
    $expectedResult['lefttier1uri7'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 7',
      '#default_value' => 'Setting: lefttier1uri7',
      '#group' => 'lefttier1link7'
    );
    // Tier2 loop - 1
    $expectedResult['tier1link1tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link1'
    );
    $expectedResult['tier1link1tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link1tier2title1',
      '#group' => 'tier1link1tier2link1'
    );
    $expectedResult['tier1link1tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link1tier2uri1',
      '#group' => 'tier1link1tier2link1'
    );
    $expectedResult['tier1link1tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link1'
    );
    $expectedResult['tier1link1tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link1tier2title2',
      '#group' => 'tier1link1tier2link2'
    );
    $expectedResult['tier1link1tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link1tier2uri2',
      '#group' => 'tier1link1tier2link2'
    );
    $expectedResult['tier1link1tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link1'
    );
    $expectedResult['tier1link1tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link1tier2title3',
      '#group' => 'tier1link1tier2link3'
    );
    $expectedResult['tier1link1tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link1tier2uri3',
      '#group' => 'tier1link1tier2link3'
    );
    $expectedResult['tier1link1tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link1'
    );
    $expectedResult['tier1link1tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link1tier2title4',
      '#group' => 'tier1link1tier2link4'
    );
    $expectedResult['tier1link1tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link1tier2uri4',
      '#group' => 'tier1link1tier2link4'
    );
    $expectedResult['tier1link1tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link1'
    );
    $expectedResult['tier1link1tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link1tier2title5',
      '#group' => 'tier1link1tier2link5'
    );
    $expectedResult['tier1link1tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link1tier2uri5',
      '#group' => 'tier1link1tier2link5'
    );

    // Tier2 loop - 2
    $expectedResult['tier1link2tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link2'
    );
    $expectedResult['tier1link2tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link2tier2title1',
      '#group' => 'tier1link2tier2link1'
    );
    $expectedResult['tier1link2tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link2tier2uri1',
      '#group' => 'tier1link2tier2link1'
    );
    $expectedResult['tier1link2tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link2'
    );
    $expectedResult['tier1link2tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link2tier2title2',
      '#group' => 'tier1link2tier2link2'
    );
    $expectedResult['tier1link2tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link2tier2uri2',
      '#group' => 'tier1link2tier2link2'
    );
    $expectedResult['tier1link2tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link2'
    );
    $expectedResult['tier1link2tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link2tier2title3',
      '#group' => 'tier1link2tier2link3'
    );
    $expectedResult['tier1link2tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link2tier2uri3',
      '#group' => 'tier1link2tier2link3'
    );
    $expectedResult['tier1link2tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link2'
    );
    $expectedResult['tier1link2tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link2tier2title4',
      '#group' => 'tier1link2tier2link4'
    );
    $expectedResult['tier1link2tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link2tier2uri4',
      '#group' => 'tier1link2tier2link4'
    );
    $expectedResult['tier1link2tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link2'
    );
    $expectedResult['tier1link2tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link2tier2title5',
      '#group' => 'tier1link2tier2link5'
    );
    $expectedResult['tier1link2tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link2tier2uri5',
      '#group' => 'tier1link2tier2link5'
    );
    // Tier2 loop - 3
    $expectedResult['tier1link3tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link3'
    );
    $expectedResult['tier1link3tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link3tier2title1',
      '#group' => 'tier1link3tier2link1'
    );
    $expectedResult['tier1link3tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link3tier2uri1',
      '#group' => 'tier1link3tier2link1'
    );
    $expectedResult['tier1link3tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link3'
    );
    $expectedResult['tier1link3tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link3tier2title2',
      '#group' => 'tier1link3tier2link2'
    );
    $expectedResult['tier1link3tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link3tier2uri2',
      '#group' => 'tier1link3tier2link2'
    );
    $expectedResult['tier1link3tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link3'
    );
    $expectedResult['tier1link3tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link3tier2title3',
      '#group' => 'tier1link3tier2link3'
    );
    $expectedResult['tier1link3tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link3tier2uri3',
      '#group' => 'tier1link3tier2link3'
    );
    $expectedResult['tier1link3tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link3'
    );
    $expectedResult['tier1link3tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link3tier2title4',
      '#group' => 'tier1link3tier2link4'
    );
    $expectedResult['tier1link3tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link3tier2uri4',
      '#group' => 'tier1link3tier2link4'
    );
    $expectedResult['tier1link3tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link3'
    );
    $expectedResult['tier1link3tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link3tier2title5',
      '#group' => 'tier1link3tier2link5'
    );
    $expectedResult['tier1link3tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link3tier2uri5',
      '#group' => 'tier1link3tier2link5'
    );
    // Tier2 loop - 4
    $expectedResult['tier1link4tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link4'
    );
    $expectedResult['tier1link4tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link4tier2title1',
      '#group' => 'tier1link4tier2link1'
    );
    $expectedResult['tier1link4tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link4tier2uri1',
      '#group' => 'tier1link4tier2link1'
    );
    $expectedResult['tier1link4tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link4'
    );
    $expectedResult['tier1link4tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link4tier2title2',
      '#group' => 'tier1link4tier2link2'
    );
    $expectedResult['tier1link4tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link4tier2uri2',
      '#group' => 'tier1link4tier2link2'
    );
    $expectedResult['tier1link4tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link4'
    );
    $expectedResult['tier1link4tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link4tier2title3',
      '#group' => 'tier1link4tier2link3'
    );
    $expectedResult['tier1link4tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link4tier2uri3',
      '#group' => 'tier1link4tier2link3'
    );
    $expectedResult['tier1link4tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link4'
    );
    $expectedResult['tier1link4tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link4tier2title4',
      '#group' => 'tier1link4tier2link4'
    );
    $expectedResult['tier1link4tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link4tier2uri4',
      '#group' => 'tier1link4tier2link4'
    );
    $expectedResult['tier1link4tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link4'
    );
    $expectedResult['tier1link4tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link4tier2title5',
      '#group' => 'tier1link4tier2link5'
    );
    $expectedResult['tier1link4tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link4tier2uri5',
      '#group' => 'tier1link4tier2link5'
    );
    // Tier2 loop - 5
    $expectedResult['tier1link5tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link5'
    );
    $expectedResult['tier1link5tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link5tier2title1',
      '#group' => 'tier1link5tier2link1'
    );
    $expectedResult['tier1link5tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link5tier2uri1',
      '#group' => 'tier1link5tier2link1'
    );
    $expectedResult['tier1link5tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link5'
    );
    $expectedResult['tier1link5tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link5tier2title2',
      '#group' => 'tier1link5tier2link2'
    );
    $expectedResult['tier1link5tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link5tier2uri2',
      '#group' => 'tier1link5tier2link2'
    );
    $expectedResult['tier1link5tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link5'
    );
    $expectedResult['tier1link5tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link5tier2title3',
      '#group' => 'tier1link5tier2link3'
    );
    $expectedResult['tier1link5tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link5tier2uri3',
      '#group' => 'tier1link5tier2link3'
    );
    $expectedResult['tier1link5tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link5'
    );
    $expectedResult['tier1link5tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link5tier2title4',
      '#group' => 'tier1link5tier2link4'
    );
    $expectedResult['tier1link5tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link5tier2uri4',
      '#group' => 'tier1link5tier2link4'
    );
    $expectedResult['tier1link5tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link5'
    );
    $expectedResult['tier1link5tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link5tier2title5',
      '#group' => 'tier1link5tier2link5'
    );
    $expectedResult['tier1link5tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link5tier2uri5',
      '#group' => 'tier1link5tier2link5'
    );
    // Tier2 loop - 6
    $expectedResult['tier1link6tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link6'
    );
    $expectedResult['tier1link6tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link6tier2title1',
      '#group' => 'tier1link6tier2link1'
    );
    $expectedResult['tier1link6tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link6tier2uri1',
      '#group' => 'tier1link6tier2link1'
    );
    $expectedResult['tier1link6tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link6'
    );
    $expectedResult['tier1link6tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link6tier2title2',
      '#group' => 'tier1link6tier2link2'
    );
    $expectedResult['tier1link6tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link6tier2uri2',
      '#group' => 'tier1link6tier2link2'
    );
    $expectedResult['tier1link6tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link6'
    );
    $expectedResult['tier1link6tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link6tier2title3',
      '#group' => 'tier1link6tier2link3'
    );
    $expectedResult['tier1link6tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link6tier2uri3',
      '#group' => 'tier1link6tier2link3'
    );
    $expectedResult['tier1link6tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link6'
    );
    $expectedResult['tier1link6tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link6tier2title4',
      '#group' => 'tier1link6tier2link4'
    );
    $expectedResult['tier1link6tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link6tier2uri4',
      '#group' => 'tier1link6tier2link4'
    );
    $expectedResult['tier1link6tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link6'
    );
    $expectedResult['tier1link6tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link6tier2title5',
      '#group' => 'tier1link6tier2link5'
    );
    $expectedResult['tier1link6tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link6tier2uri5',
      '#group' => 'tier1link6tier2link5'
    );
    // Tier2 loop - 7
    $expectedResult['tier1link7tier2link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 1',
      '#open' => true,
      '#group' => 'lefttier1link7'
    );
    $expectedResult['tier1link7tier2title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: tier1link7tier2title1',
      '#group' => 'tier1link7tier2link1'
    );
    $expectedResult['tier1link7tier2uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: tier1link7tier2uri1',
      '#group' => 'tier1link7tier2link1'
    );
    $expectedResult['tier1link7tier2link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 2',
      '#open' => false,
      '#group' => 'lefttier1link7'
    );
    $expectedResult['tier1link7tier2title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: tier1link7tier2title2',
      '#group' => 'tier1link7tier2link2'
    );
    $expectedResult['tier1link7tier2uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: tier1link7tier2uri2',
      '#group' => 'tier1link7tier2link2'
    );
    $expectedResult['tier1link7tier2link3'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 3',
      '#open' => false,
      '#group' => 'lefttier1link7'
    );
    $expectedResult['tier1link7tier2title3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 3',
      '#default_value' => 'Setting: tier1link7tier2title3',
      '#group' => 'tier1link7tier2link3'
    );
    $expectedResult['tier1link7tier2uri3'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 3',
      '#default_value' => 'Setting: tier1link7tier2uri3',
      '#group' => 'tier1link7tier2link3'
    );
    $expectedResult['tier1link7tier2link4'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 4',
      '#open' => false,
      '#group' => 'lefttier1link7'
    );
    $expectedResult['tier1link7tier2title4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 4',
      '#default_value' => 'Setting: tier1link7tier2title4',
      '#group' => 'tier1link7tier2link4'
    );
    $expectedResult['tier1link7tier2uri4'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 4',
      '#default_value' => 'Setting: tier1link7tier2uri4',
      '#group' => 'tier1link7tier2link4'
    );
    $expectedResult['tier1link7tier2link5'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Tier2 Header Link 5',
      '#open' => false,
      '#group' => 'lefttier1link7'
    );
    $expectedResult['tier1link7tier2title5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 5',
      '#default_value' => 'Setting: tier1link7tier2title5',
      '#group' => 'tier1link7tier2link5'
    );
    $expectedResult['tier1link7tier2uri5'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 5',
      '#default_value' => 'Setting: tier1link7tier2uri5',
      '#group' => 'tier1link7tier2link5'
    );
    $expectedResult['righttier1link1'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Right Tier1 Header Link 1',
      '#open' => true,
      '#group' => 'rightlinks'
    );
    $expectedResult['righttier1title1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 1',
      '#default_value' => 'Setting: righttier1title1',
      '#group' => 'righttier1link1'
    );
    $expectedResult['righttier1uri1'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 1',
      '#default_value' => 'Setting: righttier1uri1',
      '#group' => 'righttier1link1'
    );
    $expectedResult['righttier1link2'] = array(
      '#type'  => 'details',
      '#title' => 'Drupal Teed: Right Tier1 Header Link 2',
      '#open' => false,
      '#group' => 'rightlinks'
    );
    $expectedResult['righttier1title2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: Text for link 2',
      '#default_value' => 'Setting: righttier1title2',
      '#group' => 'righttier1link2'
    );
    $expectedResult['righttier1uri2'] = array(
      '#type'  => 'textfield',
      '#title' => 'Drupal Teed: URL for link 2',
      '#default_value' => 'Setting: righttier1uri2',
      '#group' => 'righttier1link2'
    );

    $result = $this->mockHelper->buildHeaderThemeSettings($form);
    $this->assertEquals($expectedResult, $result);
  }

  public function testIsMicrositeHeroPageNode_isEmpty() {
    $variables = array();
    $result = $this->helper->isMicrositeHeroPage($variables);
    $this->assertFalse($result);
  }

  public function testIsMicrositeHeroPageNode_isString() {
    $variables = array();
    $variables['node'] = 'blah';

    $nodeArray['ds_switch'][0]['value'] = 'microsites_hero';
    $mockedNode = $this->getMockBuilder('MockEntity')
                       ->setConstructorArgs(array())
                       ->getMock();

    $mockedNode->expects($this->exactly(2))
               ->method('toArray')
               ->will($this->returnValue($nodeArray));

    $this->mockHelper->expects($this->once())
                     ->method('drupalNodeLoad')
                     ->will($this->returnValue($mockedNode));

    $result = $this->mockHelper->isMicrositeHeroPage($variables);
    $this->assertTrue($result);
  }

  public function testIsMicrositeHeroPage_happy() {
    $variables = array();
    $nodeArray = array();
    $nodeArray['ds_switch'][0]['value'] = 'microsites_hero';
    $nodeEntity = $this->getMockBuilder('MockEntity')
                       ->setConstructorArgs(array())
                       ->getMock();
    $nodeEntity->expects($this->exactly(2))
               ->method('toArray')
               ->will($this->returnValue($nodeArray));
    $variables['node'] = $nodeEntity;

    $result = $this->mockHelper->isMicrositeHeroPage($variables);
    $this->assertTrue($result);
  }

  public function testIsMicrositeHeroPage_sadNotHeroPage() {
    $variables = array();
    $nodeArray = array();
    $nodeArray['ds_switch'][0]['value'] = 'microsites_not_hero_aww';
    $nodeEntity = $this->getMockBuilder('MockEntity')
                       ->setConstructorArgs(array())
                       ->getMock();
    $nodeEntity->expects($this->exactly(2))
               ->method('toArray')
               ->will($this->returnValue($nodeArray));
    $variables['node'] = $nodeEntity;

    $result = $this->mockHelper->isMicrositeHeroPage($variables);
    $this->assertFalse($result);
  }

  public function testIsMicrositeHeroPage_sadNotSet() {
    $variables = array();
    $nodeArray = array();
    $nodeArray['value'] = 'microsites_hero';
    $nodeEntity = $this->getMockBuilder('MockEntity')
                       ->setConstructorArgs(array())
                       ->getMock();
    $nodeEntity->expects($this->once())
               ->method('toArray')
               ->will($this->returnValue($nodeArray));
    $variables['node'] = $nodeEntity;

    $result = $this->mockHelper->isMicrositeHeroPage($variables);
    $this->assertFalse($result);
  }

  public function testPreprocessFormElementRadio_happy() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#type'] = 'radio';
    $variables['element']['#default_value'] = 'cinderella';
    $variables['element']['#attributes']['name'] = 'belle';
    $variables['element']['#attributes']['id'] = 'aurora';
    $variables['element']['#attributes']['value'] = 'cinderella';

    $expectedResult = array();
    $expectedResult['name'] = 'belle';
    $expectedResult['id'] = 'aurora';
    $expectedResult['value'] = 'cinderella';
    $expectedResult['checked'] = 'checked';

    $result = $this->helper->preprocessFormElementRadio($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testPreprocessFormElementRadio_defaultValueNotEqualToValue() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#type'] = 'radio';
    $variables['element']['#default_value'] = 'cinderella';
    $variables['element']['#attributes']['name'] = 'belle';
    $variables['element']['#attributes']['id'] = 'aurora';
    $variables['element']['#attributes']['value'] = 'ariel';

    $expectedResult = array();
    $expectedResult['name'] = 'belle';
    $expectedResult['id'] = 'aurora';
    $expectedResult['value'] = 'ariel';

    $result = $this->helper->preprocessFormElementRadio($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testPreprocessFormElementRadio_typeNotRadio() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#type'] = 'mirror';
    $variables['element']['#default_value'] = 'cinderella';
    $variables['element']['#attributes']['name'] = 'belle';
    $variables['element']['#attributes']['id'] = 'aurora';
    $variables['element']['#attributes']['value'] = 'ariel';

    $expectedResult = array();

    $result = $this->helper->preprocessFormElementRadio($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testPreprocessFieldsets() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#title'] = 'fieldset_title';

    $variablesAfterPrefix = $variables;
    $variablesAfterPrefix[] = 'prefix';
    $variablesAfterSuffix = $variablesAfterPrefix;
    $variablesAfterSuffix[] = 'suffix';
    $variablesAfterSuffix['fieldset_title'] = 'fieldset_title';

    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                      ->setMethods(array('replaceDefaultDescriptionHtml'))
                      ->disableOriginalConstructor()
                      ->getMock();

    $replaceDefaultDescriptionHtmlCalls = array(
      array($variables, 'prefix', $variablesAfterPrefix),
      array($variablesAfterPrefix, 'suffix', $variablesAfterSuffix)
    );

    $myMockHelper->expects($this->exactly(2))
         ->method('replaceDefaultDescriptionHtml')
         ->will($this->returnValueMap($replaceDefaultDescriptionHtmlCalls));

    $result = $myMockHelper->preprocessFieldsets($variables);
    $this->assertSame($variablesAfterSuffix, $result);
  }

  public function testMatchInline_checkboxTrue() {
    $className = 'checkbox-inline';
    $expectedResult = true;
    $result = $this->helper->matchInline($className);
    $this->assertEquals($expectedResult, $result);
  }

  public function testMatchInline_radioTrue() {
    $className = 'radio-inline';
    $expectedResult = true;
    $result = $this->helper->matchInline($className);
    $this->assertEquals($expectedResult, $result);
  }

  public function testMatchInline_neitherFalse() {
    $className = 'snow-white';
    $expectedResult = false;
    $result = $this->helper->matchInline($className);
    $this->assertEquals($expectedResult, $result);
  }

  public function testGetLeftTier1Link() {
    $index = 'princess';
    $titleString = 'lefttier1title' . $index;
    $urlString = 'lefttier1uri' . $index;

    $drupalThemeGetSettingCalls = array(
      array($titleString, 'rapunzel-princess'),
      array($urlString, 'mulan-princess')
    );

    $this->mockHelper->expects($this->exactly(2))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();
    $expectedResult['title'] = 'rapunzel-princess';
    $expectedResult['url'] = 'mulan-princess';
    $expectedResult['current_page_classes'] = '';

    $result = $this->mockHelper->getLeftTier1Link($index);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetLegalFooterContent_footerEmpty() {

    $this->mockHelper->expects($this->once())
                     ->method('getDefaultLegalFooterContent')
                     ->will($this->returnValue('I am the footer.'));

    $this->mockHelper->expects($this->once())
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue(''));

    $expectedResult = 'I am the footer.';
    $result = $this->mockHelper->getLegalFooterContent();
    $this->assertEquals($expectedResult, $result);
  }

  public function testGetLegalFooterContent_footerNotEmpty() {
    $this->mockHelper->expects($this->once())
                     ->method('getDefaultLegalFooterContent')
                     ->will($this->returnValue('I am the footer.'));

    $this->mockHelper->expects($this->exactly(2))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue('I am the NEW footer!'));

    $expectedResult = 'I am the NEW footer!';
    $result = $this->mockHelper->getLegalFooterContent();
    $this->assertEquals($expectedResult, $result);
  }

  public function testGetDefaultFooterLinks() {
    $expectedResult = array(
      1 => array('title' => 'Terms of Use', 'url' => 'https://www.principal.com/terms-of-use'),
      2 => array('title' => 'Disclosures', 'url' => 'https://www.principal.com/products-services-disclosures'),
      3 => array('title' => 'Privacy', 'url' => 'https://www.principal.com/privacy-policies'),
      4 => array('title' => 'Security', 'url' => 'https://www.principal.com/security-policies'),
      5 => array('title' => 'Report Fraud', 'url' => 'https://www.principal.com/about-us/our-company/policies/report-fraud-or-unethical-conduct'),
      6 => array('title' => 'Site Map', 'url' => 'https://www.principal.com/site-map'),
      7 => array('title' => '', 'url' => '')
    );

    $result = $this->helper->getDefaultFooterLinks();
    $this->assertSame($expectedResult, $result);
  }

  public function testGetDefaultLegalFooterContent() {
    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('phpStrftime'))
                         ->disableOriginalConstructor()
                         ->getMock();
    $myMockHelper->expects($this->once())
                 ->method('phpStrftime')
                 ->with('%Y')
                 ->will($this->returnValue('2017'));
    $expectedResult = '<p>© 2017, Principal Financial Services, Inc.</p><p>Securities offered through Principal Securities, Inc., <a href="http://www.sipc.org">member SIPC</a></p>';

    $result = $myMockHelper->getDefaultLegalFooterContent();
    $this->assertEquals($expectedResult, $result);
  }

  public function testHasCobrandedLogo_true() {
    $this->mockHelper->expects($this->once())
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue('Logo is set.'));

    $expectedResult = true;

    $result = $this->mockHelper->hasCoBrandedLogo();
    $this->assertEquals($expectedResult, $result);
  }

  public function testHasCobrandedLogo_false() {
    $this->mockHelper->expects($this->once())
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue(null));

    $expectedResult = false;

    $result = $this->mockHelper->hasCoBrandedLogo();
    $this->assertEquals($expectedResult, $result);
  }

  public function testhasCoBrandedLogo2_true() {
    $this->mockHelper->expects($this->once())
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue('Logo is set.'));

    $expectedResult = true;

    $result = $this->mockHelper->hasCoBrandedLogo();
    $this->assertEquals($expectedResult, $result);
  }

  public function testhasCoBrandedLogo2_false() {
    $this->mockHelper->expects($this->once())
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue(null));

    $expectedResult = false;

    $result = $this->mockHelper->hasCoBrandedLogo();
    $this->assertEquals($expectedResult, $result);
  }

  public function testBuildHorizonSettingGroups() {
    $form = array();

    $drupalTCalls = array(
      array('Horizon Settings', 'THorizon TSettings'),
      array('Header', 'THeader'),
      array('Footer', 'TFooter'),
      array('Co-branded logo', 'TCo-branded tlogo')
    );

    $this->mockHelper->expects($this->exactly(4))
                     ->method('drupalT')
                     ->will($this->returnValueMap($drupalTCalls));

    $expectedResult = array();
    $expectedResult['horizon'] = array(
      '#type' => 'vertical_tabs',
      '#attached' => ['library' => ['horizon/theme-settings']],
      '#prefix' => '<h2><small>THorizon TSettings</small></h2>',
      '#weight' => -20
    );
    $expectedResult['header'] = array(
      '#type' => 'details',
      '#title' => 'THeader',
      '#group' => 'horizon'
    );
    $expectedResult['footer'] = array(
      '#type' => 'details',
      '#title' => 'TFooter',
      '#group' => 'horizon'
    );
    $expectedResult['cobrand'] = array(
      '#type' => 'details',
      '#title' => 'TCo-branded tlogo',
      '#group' => 'horizon'
    );

    $result = $this->mockHelper->buildHorizonSettingGroups($form);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetRightTier1Links_notEmpty() {
    $index = 'charming';

    $drupalThemeGetSettingCalls = array(
      array('righttier1title1', 'jasmine'),
      array('righttier1uri1', 'arabian-peninsula'),
      array('righttier1title2', 'tiana'),
      array('righttier1uri2', 'new-orleans')
    );

    $this->mockHelper->expects($this->exactly(4))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();
    $expectedResult[0]['title'] = 'jasmine';
    $expectedResult[0]['url'] = 'arabian-peninsula';
    $expectedResult[1]['title'] = 'tiana';
    $expectedResult[1]['url'] = 'new-orleans';

    $result = $this->mockHelper->getRightTier1Links($index);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetRightTier1Links_allEmpty() {
    $index = 'charming';

    $drupalThemeGetSettingCalls = array(
      array('righttier1title1', ''),
      array('righttier1uri1', ''),
      array('righttier1title2', ''),
      array('righttier1uri2', '')
    );

    $this->mockHelper->expects($this->exactly(4))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();

    $result = $this->mockHelper->getRightTier1Links($index);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetRightTier1Links_urisEmpty() {
    $index = 'charming';

    $drupalThemeGetSettingCalls = array(
      array('righttier1title1', 'jasmine'),
      array('righttier1uri1', ''),
      array('righttier1title2', 'tiana'),
      array('righttier1uri2', '')
    );

    $this->mockHelper->expects($this->exactly(4))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();

    $result = $this->mockHelper->getRightTier1Links($index);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetRightTier1Links_titlesEmpty() {
    $index = 'charming';

    $drupalThemeGetSettingCalls = array(
      array('righttier1title1', ''),
      array('righttier1uri1', 'arabian-peninsula'),
      array('righttier1title2', ''),
      array('righttier1uri2', 'new-orleans')
    );

    $this->mockHelper->expects($this->exactly(4))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();

    $result = $this->mockHelper->getRightTier1Links($index);
    $this->assertSame($expectedResult, $result);
  }

  public function testBuildCoBrandedLogoThemeSettings() {
    $form = array();

    $drupalTCalls = array(
      array('Tier 1 Co-Branded Logo Image', 'Glass Slipper'),
      array('This logo will replace the Principal logo in the tier 1 header.', 'Mirror Mirror'),
      array('Tier 2 Co-Branded Logo Image', 'Floating Lantern'),
      array('This logo will replace the Principal logo in the tier 2 header.', 'Magic Lamp')
    );

    $this->mockHelper->expects($this->exactly(4))
                     ->method('drupalT')
                     ->will($this->returnValueMap($drupalTCalls));

    $this->mockHelper->expects($this->exactly(2))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValue('poison-apple'));

    $this->mockHelper->expects($this->exactly(2))
                     ->method('drupalFileDefaultScheme')
                     ->will($this->returnValue('enchanted-rose'));

    $expectedResult['cobrand']['uploadlogo'] = array(
      '#title' => 'Glass Slipper',
      '#type' => 'managed_file',
      '#description' => 'Mirror Mirror',
      '#default_value' => 'poison-apple',
      '#upload_location' => 'enchanted-rose://cobranded-logo/',
      '#group' => 'cobrand',
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      )
    );
    $expectedResult['cobrand']['uploadlogo2'] = array(
      '#title' => 'Floating Lantern',
      '#type' => 'managed_file',
      '#description' => 'Magic Lamp',
      '#default_value' => 'poison-apple',
      '#upload_location' => 'enchanted-rose://cobranded-logo/',
      '#group' => 'cobrand',
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      )
    );

    $result = $this->mockHelper->buildCoBrandedLogoThemeSettings($form);
    $this->assertSame($expectedResult, $result);
  }

  public function testGetFooterLinks_notEmpty() {
    $link1 = array('title' => 'Giselle', 'url' => 'Dragon');
    $link2 = array('title' => 'Merida', 'url' => 'Bears');
    $link3 = array('title' => 'Moana', 'url' => 'Maui');

    $this->mockHelper->expects($this->once())
                     ->method('getDefaultFooterLinks')
                     ->will($this->returnValue(array(1 => $link1, 2 => $link2, 3 => $link3)));

    $drupalThemeGetSettingCalls = array(
      array('footertitle1', 'Kristoff'),
      array('footeruri1', 'Sven'),
      array('footertitle2', 'Flynn'),
      array('footeruri2', 'Pascal'),
      array('footertitle3', 'Aladdin'),
      array('footeruri3', 'Abu')
    );

    $this->mockHelper->expects($this->exactly(6))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();
    $expectedResult[0] = array('title' => 'Kristoff', 'url' => 'Sven');
    $expectedResult[1] = array('title' => 'Flynn', 'url' => 'Pascal');
    $expectedResult[2] = array('title' => 'Aladdin', 'url' => 'Abu');

    $result = $this->mockHelper->getFooterLinks();
    $this->assertSame($expectedResult, $result);
  }

  public function testGetFooterLinks_emptyTitleAndUrl() {
    $link1 = array('title' => 'Giselle', 'url' => 'Dragon');
    $link2 = array('title' => 'Merida', 'url' => 'Bears');
    $link3 = array('title' => 'Moana', 'url' => 'Maui');

    $this->mockHelper->expects($this->once())
                     ->method('getDefaultFooterLinks')
                     ->will($this->returnValue(array(1 => $link1, 2 => $link2, 3 => $link3)));

    $drupalThemeGetSettingCalls = array(
      array('footertitle1', ''),
      array('footeruri1', ''),
      array('footertitle2', ''),
      array('footeruri2', ''),
      array('footertitle3', ''),
      array('footeruri3', '')
    );

     $this->mockHelper->expects($this->exactly(6))
                     ->method('drupalThemeGetSetting')
                     ->will($this->returnValueMap($drupalThemeGetSettingCalls));

    $expectedResult = array();
    $expectedResult[0] = array('title' => 'Giselle', 'url' => 'Dragon');
    $expectedResult[1] = array('title' => 'Merida', 'url' => 'Bears');
    $expectedResult[2] = array('title' => 'Moana', 'url' => 'Maui');

    $result = $this->mockHelper->getFooterLinks();
    $this->assertSame($expectedResult, $result);
  }

  public function testGetCoBrandedLogoURL_happy() {
    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('drupalFileCreateUrl', 'drupalFileLoad', 'drupalThemeGetSetting', 'hasCoBrandedLogo'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $myMockHelper->expects($this->once())
                 ->method('hasCoBrandedLogo')
                 ->will($this->returnValue(true));

    $uri = $this->getMockBuilder('MockField')
                ->setConstructorArgs(array())
                ->getMock();

    $uri->expects($this->once())
        ->method('getValue')
        ->will($this->returnValue(array(array('value' => 'cobranded_logo_uri'))));

    $mockCobrandedLogo = $this->getMockBuilder('MockFile')
                              ->setConstructorArgs(array($uri))
                              ->getMock();

    $myMockHelper->expects($this->once())
                 ->method('drupalFileLoad')
                 ->with('woohoo theme setting')
                 ->will($this->returnValue($mockCobrandedLogo));

    $themeGetSettingResult = array(0 => 'woohoo theme setting');

    $myMockHelper->expects($this->once())
                 ->method('drupalThemeGetSetting')
                 ->with('uploadlogo')
                 ->will($this->returnValue($themeGetSettingResult));

    $myMockHelper->expects($this->once())
                 ->method('drupalFileCreateUrl')
                 ->with('cobranded_logo_uri')
                 ->will($this->returnValue('huzzah'));

    $result = $myMockHelper->getCoBrandedLogoURL();
    $this->assertEquals('huzzah', $result);
  }

  public function testGetCoBrandedLogoURL_sad() {
    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('drupalFileCreateUrl', 'drupalFileLoad', 'drupalThemeGetSetting', 'hasCoBrandedLogo'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $myMockHelper->expects($this->once())
                 ->method('hasCoBrandedLogo')
                 ->will($this->returnValue(false));

    $myMockHelper->expects($this->never())
                 ->method('drupalFileLoad');

    $myMockHelper->expects($this->never())
                 ->method('drupalThemeGetSetting');

    $myMockHelper->expects($this->never())
                 ->method('drupalFileCreateUrl');

    $result = $myMockHelper->getCoBrandedLogoURL();
    $this->assertEquals('', $result);
  }

  public function testgetCoBrandedLogo2URL_happy() {
    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('drupalFileCreateUrl', 'drupalFileLoad', 'drupalThemeGetSetting', 'hasCoBrandedLogo2'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $myMockHelper->expects($this->once())
                 ->method('hasCoBrandedLogo2')
                 ->will($this->returnValue(true));

    $uri = $this->getMockBuilder('MockField')
                ->setConstructorArgs(array())
                ->getMock();

    $uri->expects($this->once())
        ->method('getValue')
        ->will($this->returnValue(array(array('value' => 'cobranded_logo_uri'))));

    $mockCobrandedLogo = $this->getMockBuilder('MockFile')
                              ->setConstructorArgs(array($uri))
                              ->getMock();

    $myMockHelper->expects($this->once())
                 ->method('drupalFileLoad')
                 ->with('woohoo theme setting')
                 ->will($this->returnValue($mockCobrandedLogo));

    $themeGetSettingResult = array(0 => 'woohoo theme setting');

    $myMockHelper->expects($this->once())
                 ->method('drupalThemeGetSetting')
                 ->with('uploadlogo2')
                 ->will($this->returnValue($themeGetSettingResult));

    $myMockHelper->expects($this->once())
                 ->method('drupalFileCreateUrl')
                 ->with('cobranded_logo_uri')
                 ->will($this->returnValue('huzzah'));

    $result = $myMockHelper->getCoBrandedLogo2URL();
    $this->assertEquals('huzzah', $result);
  }

  public function testgetCoBrandedLogo2URL_sad() {
    $myMockHelper = $this->getMockBuilder('Drupal\horizon\Horizon')
                         ->setMethods(array('drupalFileCreateUrl', 'drupalFileLoad', 'drupalThemeGetSetting', 'hasCoBrandedLogo2'))
                         ->disableOriginalConstructor()
                         ->getMock();

    $myMockHelper->expects($this->once())
                 ->method('hasCoBrandedLogo2')
                 ->will($this->returnValue(false));

    $myMockHelper->expects($this->never())
                 ->method('drupalFileLoad');

    $myMockHelper->expects($this->never())
                 ->method('drupalThemeGetSetting');

    $myMockHelper->expects($this->never())
                 ->method('drupalFileCreateUrl');

    $result = $myMockHelper->getCoBrandedLogo2URL();
    $this->assertEquals('', $result);
  }

  public function testSetFormElementAttributeClasses_happy() {
    $variables = array();
    $variables['attributes']['class'] = array(0 => 'mickey', 1 => 'form-inline', 2 => 'mouse');
    $expectedResult = array();
    $expectedResult['attributes']['class'] = array(0 => 'mickey', 2 => 'mouse', 'form-group');
    $result = $this->helper->setFormElementAttributeClasses($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testSetFormElementAttributeClasses_noInlineClass() {
    $variables = array();
    $variables['attributes']['class'] = array(0 => 'mickey', 1 => 'mouse');
    $expectedResult = array();
    $expectedResult['attributes']['class'] = array(0 => 'mickey', 1 => 'mouse', 2 => 'form-group');
    $result = $this->helper->setFormElementAttributeClasses($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testSetFormElementAttributeClasses_attributesClassArrayNotSet() {
    $variables = array();
    $expectedResult = array();
    $expectedResult['attributes']['class'] = array('form-group');
    $result = $this->helper->setFormElementAttributeClasses($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testIsExternalPath_true() {
    $this->mockHelper->expects($this->once())
                 ->method('drupalGetRequestUri')
                 ->will($this->returnValue('/external/footer'));

    $result = $this->mockHelper->isExternalPath();
    $this->assertEquals(true, $result);
  }

  public function testIsExternalPath_false() {
    $this->mockHelper->expects($this->once())
                 ->method('drupalGetRequestUri')
                 ->will($this->returnValue('/random/path'));

    $result = $this->mockHelper->isExternalPath();
    $this->assertEquals(false, $result);
  }

  public function testGetCurrentPath_notFrontPage() {
    $this->mockHelper->expects($this->once())
                     ->method('drupalGetRequestUri')
                     ->will($this->returnValue('/not/front/page'));
    $this->mockHelper->expects($this->never())
                     ->method('drupalGetConfig');

    $result = $this->mockHelper->getCurrentPath();
    $this->assertEquals('/not/front/page', $result);
  }

  public function testGetCurrentPath_isFrontPage() {
    $this->mockHelper->expects($this->once())
                     ->method('drupalGetRequestUri')
                     ->will($this->returnValue('/'));
    $mockConfig = $this->getMockBuilder('MockConfig')
                       ->setConstructorArgs(array())
                       ->getMock();
    $mockConfig->expects($this->once())
               ->method('get')
               ->with('page.front')
               ->will($this->returnValue('/site/frontpage/var'));
    $this->mockHelper->expects($this->once())
                     ->method('drupalGetConfig')
                     ->with('system.site')
                     ->will($this->returnValue($mockConfig));

    $result = $this->mockHelper->getCurrentPath();
    $this->assertEquals('/site/frontpage/var', $result);
  }

  public function testSaveFileAsPermanent_happy() {
    $fileFromFormState = array(0 => '1');

    $mockFormState = $this->getMockBuilder('MockFormState')
                          ->setConstructorArgs(array())
                          ->getMock();

    $mockFormState->expects($this->exactly(2))
                  ->method('getValue')
                  ->with('uploadfield')
                  ->will($this->returnValue($fileFromFormState));

    $mockFile = $this->getMockBuilder('MockFile')
                     ->setConstructorArgs(array())
                     ->getMock();

    $mockFile->expects($this->once())
             ->method('setPermanent');

    $mockFile->expects($this->once())
             ->method('save');

    $this->mockHelper->expects($this->once())
                     ->method('drupalFileLoad')
                     ->with('1')
                     ->will($this->returnValue($mockFile));

    $this->mockHelper->saveFileAsPermanent($mockFormState, 'uploadfield');
  }

  public function testSaveFileAsPermanent_emptyFile() {
    $fileFromFormState = array();

    $mockFormState = $this->getMockBuilder('MockFormState')
                          ->setConstructorArgs(array())
                          ->getMock();

    $mockFormState->expects($this->once())
                  ->method('getValue')
                  ->with('uploadfield')
                  ->will($this->returnValue($fileFromFormState));

    $this->mockHelper->expects($this->never())
                     ->method('drupalFileLoad');

    $this->mockHelper->saveFileAsPermanent($mockFormState, 'uploadfield');
  }

}

?>
