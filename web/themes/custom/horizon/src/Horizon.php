<?php
/**
 * @file
 * Contains \Drupal\horizon\Horizon.
 * Class for more complex theme functions.
 */

namespace Drupal\horizon;

use Drupal\bootstrap\Bootstrap;
use Principal\traits\DrupalFunctions;
use Principal\traits\Construct;
use Principal\traits\AttributeFunctions;
use Principal\traits\MarkupFunctions;
use Principal\traits\UrlFunctions;
use Principal\traits\PhpFunctions;

/**
 * The primary class for the Horizon subtheme.
 *
 * Provides many helper methods.
 *
 */
class Horizon extends Bootstrap {

  use Construct;
  use AttributeFunctions;
  use MarkupFunctions;
  use UrlFunctions;
  use DrupalFunctions;
  use PhpFunctions;

  /**
   * Adds a callback to an array.
   *
   * @param array $libraries
   *   An array of libraries that are attached to the page ($variables['#attached']['library'])
   * @param  string $environment
   *   The current environment (test, pilot, prod, or local)
   *
   * @return array
   *   The array of attached libraries that was passed in, with our specific libs added
   */
  public function addLibraries(array $libraries, $environment) {

    // Set environment-specific libraries
    switch ($environment) {
      case 'prod':
        $libraries[] = 'horizon/horizon-prod-css';
        $libraries[] = 'horizon/horizon-prod-js';
        break;

      case 'pilot':
        $libraries[] = 'horizon/horizon-pilot-css';
        $libraries[] = 'horizon/horizon-pilot-js';
        break;

      case 'test':
        $libraries[] = 'horizon/horizon-test-css';
        $libraries[] = 'horizon/horizon-test-js';
        break;
      // default localhost
      default:
        $libraries[] = 'horizon/horizon-prod-css';
        $libraries[] = 'horizon/horizon-prod-js';
        break;
    }

    // Add our theme css and js
    $libraries[] = 'horizon/horizon-theme-css';
    $libraries[] = 'horizon/horizon-theme-js';

    return $libraries;
  }

  /**
   * Preprocess function for form element labels
   *
   * @param array $variables
   *   An array of variables that was passed in from the preprocess function in horizon.theme
   *
   * @return array $variables
   *   The array of variables that was passed in, with our adjustments made
   *
   * @see horizon_preprocess_form_element()
   */
  public function preprocessFormElementLabel($variables) {
    $element = $variables['element'];

    // Determine if checkbox or radio elements
    $isCheckboxOrRadio = (isset($element['#type']) && ('checkbox' === $element['#type'] || 'radio' === $element['#type']));

    // If title and required marker are both empty, output no label
    if ((!isset($element['#title']) || $element['#title'] === '' && !$isCheckboxOrRadio) && empty($element['#required'])) {
      return '';
    }

    $attributes = array();

    // Style the label as class option to display inline with the element.
    if ($element['#title_display'] == 'after' && !$isCheckboxOrRadio) {
      $attributes['class'][] = $element['#type'];
    }
    // Show label only to screen readers to avoid disruption in visual flows.
    elseif ($element['#title_display'] == 'invisible') {
      $attributes['class'][] = 'sr-only';
    }

    // Add generic Bootstrap identifier class.
    $attributes['class'][] = 'control-label';
    if (isset($element['#required']) && $element['#required']) {
      $attributes['class'][] = 'is-required';
    }

    if (!empty($element['#id'])) {
      $attributes['for'] = $element['#id'];
    }

    $label = array();
    $label['attributes'] = $this->createNewAttribute($attributes);
    $label['text'] = $element['#title'];
    // Set the element_id (used for checkboxes and radios)
    $label['element_id'] = isset($attributes['for']) ? $attributes['for'] : '';

    return $label;
  }

  /**
   * Preprocess function for radio form elements
   *
   * @param array $variables
   *   An array of variables that was passed in from the preprocess function in horizon.theme
   *
   * @return array $variables
   *   The array of variables that was passed in, with our adjustments made
   *
   * @see horizon_preprocess_form_element()
   */
  public function preprocessFormElementRadio($variables) {
    $element = $variables['element'];
    $horizonRadio = array();

    if ('radio' === $element['#type']) {
      $horizonRadio['name'] = $element['#attributes']['name'];
      $horizonRadio['id'] = $element['#attributes']['id'];
      $horizonRadio['value'] = $element['#attributes']['value'];
      // Necessary to ensure the correct default is checked.
      if ($element['#default_value'] == $horizonRadio['value']) {
        $horizonRadio['checked'] = 'checked';
      }
    }

    return $horizonRadio;
  }

  /**
   * Preprocess function for form fieldsets
   *
   * @param array $variables
   *   An array of variables that was passed in from the preprocess function in horizon.theme
   *
   * @return array $variables
   *   The array of variables that was passed in, with our adjustments made
   *
   * @see horizon_preprocess_fieldset()
   */
  public function preprocessFieldsets($variables) {
    // Descriptions get set as field prefixes/suffixes on fieldsets (like radios and checkboxes),
    // and by default are output as <div class="description">the description</div>.
    // We need to replace the div with our Horizon markup
    // Check if we have a description and replace the markup with our Horizon markup
    $variables = $this->replaceDefaultDescriptionHtml($variables, 'prefix');
    $variables = $this->replaceDefaultDescriptionHtml($variables, 'suffix');
    $element = $variables['element'];

    $variables['fieldset_title'] = $element['#title'];

    return $variables;
  }

  /**
   * Preprocess function for form checkboxes elements (the container for individual checkbox elements)
   *
   * @param array $variables
   *   An array of variables that was passed in from the preprocess function in horizon.theme
   *
   * @return array $variables
   *   The array of variables that was passed in, with our adjustments made
   *
   * @see horizon_preprocess_checkboxes()
   */
  public function preprocessCheckboxes($variables) {
    $element = $variables['element'];
    $variables['attributes'] = isset($variables['attributes']) ? $variables['attributes'] : array();

    $variables['attributes']['class'][] = 'form-checkboxes';

    if (!empty($element['#attributes']['class'])) {
      $variables['attributes']['class'] = array_merge($variables['attributes']['class'], $element['#attributes']['class']);
    }

    if (isset($element['#attributes']['aria-describedby'])) {
      $variables['attributes']['aria-describedby'] = $element['#attributes']['aria-describedby'];
    }

    // inline Horizon adjustments
    foreach ($variables['attributes']['class'] as $key => $value) {
      if ($this->matchInline($value)) {
        $variables['element']['#children'] = str_replace('control-label', 'control-label ' . $value, $element['#children']);
        unset($variables['attributes']['class'][$key]);
      }
    }

    $variables['display_type'] = $element['#options_display'];

    return $variables;
  }

  /**
   * Helper function for editing the default description html that is generated by Drupal
   * for fieldsets like radios and checkboxes
   *
   * @param array $variables
   *   An array of variables that was passed in from the preprocess function in horizon.theme
   * @param string $variablesKey
   *   A string representing the key in the variables array that contins the description div
   *   that we wish to overwrite
   *
   * @return array $variables
   *   The array of variables that was passed in, with our adjustments made
   *
   * @see preprocessFieldsets()
   */
  public function replaceDefaultDescriptionHtml($variables, $variablesKey) {
    $element = $variables['element'];
    $id = $element['#attributes']['data-drupal-selector'] . '--description';

    if (isset($variables[$variablesKey]) && strpos($variables[$variablesKey], '<div class="description">') !== false) {
      $descriptionDivPattern = '/(<div class="description">)(.*)(<\/div>)/';
      $descriptionReplacementHtml = '<span id="' . $id . '" class="help-block">${2}</span>';
      $variables[$variablesKey] = $this->createMarkup(preg_replace($descriptionDivPattern, $descriptionReplacementHtml, $variables[$variablesKey]));
    }
    // Associate our help block with the field it describes by setting the aria-describedby attribute
    $variables['attributes']['aria-describedby'] = $id;

    return $variables;
  }

  /**
   * Helper function to determine if a radio or checkbox is inline
   *
   * @param string     $className The element's class
   *
   * @return boolean   True if the element is inline, false otherwise
   *
   * @see   horizon_preprocess_checkboxes()
   */
  public function matchInline($className) {

    return in_array($className, array(
      'checkbox-inline',
      'radio-inline'
    )) ? true : false;
  }

  /**
   * Helper function for cta rendering
   *
   * @param $variables   Theme variables from template_preprocess_field()
   *
   * @return  null   No return since we are editing $variables by reference
   *
   * @see  field--field-cta-content.html.twig
   * @see  template_preprocess_field
   */
  public function setCtaVariables(&$variables) {
    // Loop over the referenced entities in our CTA field and set
    // the necessary values to pass to our template
    $ctaArray = array();
    $ctas = $variables['element']['#items']->referencedEntities();
    foreach ($ctas as $cta) {
      $item = array();
      // Get body
      $bodyFieldArray = $cta->get('body')->getValue();
      $body = $bodyFieldArray[0]['value'];
      $item['body'] = $body;
      // Get title
      $titleFieldArray = $cta->get('title')->getValue();
      $title = $titleFieldArray[0]['value'];
      $item['title'] = $title;
      // Get link values
      $linkFieldArray = $cta->get('field_cta_link')->getValue();
      $item['link_url'] = '';
      $item['link_title'] = '';

      if (!empty($linkFieldArray)) {
        $linkTitle = $linkFieldArray[0]['title'];
        $linkUri = $linkFieldArray[0]['uri'];
        $urlObj = $this->fromUri($linkUri, array());
        $item['link_url'] = $urlObj->toString();
        $item['link_title'] = $linkTitle;
      }
      // Add the item to our array of CTAs
      $ctaArray[] = $item;
    }

    $variables['ctas'] = $ctaArray;
  }

  /**
   * Helper function for hero variable rendering
   *
   * @param $variables   Theme variables from template_preprocess_field()
   *
   * @return  null   No return since we are editing $variables by reference
   *
   * @see  field--field-hero-image-full-width.html.twig
   * @see  template_preprocess_field
   */
  public function setHeroVariables(&$variables) {
    $nodeArray = $variables['element']['#object']->toArray();
    $heroImg = $this->drupalFileLoad($nodeArray['field_hero_image_full_width'][0]['target_id']);
    $uriValue = isset($heroImg->uri) ? $heroImg->uri->getValue()[0]['value'] : '';
    $heroUrl = $this->drupalFileCreateUrl($uriValue);
    $variables['hero_headline'] = $nodeArray['field_hero_title'][0]['value'];
    $variables['hero_image_path'] = $heroUrl;
    $variables['hero_description'] = $this->createMarkup($nodeArray['field_hero_body'][0]['value']);
    $variables['hero_cta_link'] = $nodeArray['field_hero_cta'][0]['uri'];
    $variables['hero_cta_title'] = $nodeArray['field_hero_cta'][0]['title'];
    $variables['hero_copy_alignment_class'] = $nodeArray['field_hero_alignment'][0]['value'] == 'right' ? 'hero-copy-right' : 'hero-copy-left';
    $variables['hero_inverse_class'] = $nodeArray['field_hero_text_color'][0]['value'] == 'light' ? 'hero-inverse' : '';
  }

  /**
   * Create an array of left-aligned links for the tier1 header.
   *
   * @return array
   *   The array of left-aligned links, ready for use in our twig template (header.html.twig)
   */
  public function getLeftTier1Link($index) {
    $link = array();
    $link['title'] = $this->drupalThemeGetSetting('lefttier1title' . $index);
    $link['url'] = $this->drupalThemeGetSetting('lefttier1uri' . $index);
    $link['current_page_classes'] = '';

    return $link;
  }

  /**
   * Build the form groups for our Horizon theme settings
   *
   * @param  string $currentPath         The current path
   * @param  string $currentPathAlias    The current path alias
   * @param  string $link                The link we are currently checking
   * @param  string $parentLink          The corresponding tier1 link when we are on a tier2 link (optional)
   *
   * @return array
   *   An array of links separated by tier1 and tier2 keys, with the active classes
   *   added where applicable
   *
   * @see  getHeaderMenuLinks
   */
  public function addActiveClasses($currentPath, $currentPathAlias, $link, $parentLink = array()) {
    $linksToReturn = array();

    if (empty($parentLink)) {
      $linksToReturn['tier1'] = $link;
      $linksToReturn['tier2'] = array();
      if ((stristr($link['url'], $currentPath) || stristr($link['url'], $currentPathAlias)) && $currentPath !== "/") {
        $linksToReturn['tier1']['current_page_classes'] = 'current-page tier-one-active-page';
      }
    } else {
      $linksToReturn['tier1'] = $parentLink;
      $linksToReturn['tier2'] = $link;
      $varUrl = array();
      $varUrl = explode(".com", $link['url']);
      if (isset($varUrl[1]) && $varUrl[1] == $currentPath || $varUrl[1] == $currentPathAlias) {
        $linksToReturn['tier1']['current_page_classes'] = 'current-page tier-one-active-page';
        $linksToReturn['tier2']['current_page_classes'] = 'active-trail tier-two-active-page';
      }
    }

    return $linksToReturn;
  }

  /**
   * Make tier2 visible on all pages
   * If path is /tier1Path/somePathNotInTier2, then the tier2 menu corresponding to tier1Path is shown
   * If path is /somePathNotInMenu, then tier2 defaults to the first tier1 link's tier2 menu
   *
   * @param  string $currentPath         The current path
   * @param  string $currentPathAlias    The current path alias
   * @param  array  $tier1HeaderLinks    The tier1 links generated by getHeaderMenuLinks
   *
   * @return array    The array of tier1HeaderLinks that was passed in, with the active classes
   *                  set on the appropriate tier1 link so the tier2 menu shows up
   *
   * @see  getHeaderMenuLinks
   */
  public function addClassesForNonMenuPages($currentPath, $currentPathAlias, $tier1HeaderLinks) {
    $alreadyHasActiveClassSet = false;

    // Determine if we already have our active classes set
    foreach ($tier1HeaderLinks as $tier1HeaderLink) {
      if ($tier1HeaderLink['current_page_classes'] == 'current-page tier-one-active-page') {
        $alreadyHasActiveClassSet = true;
        break;
      }
    }

    // No active classes have been set yet, so check for a tier1 path in the current url
    if (!$alreadyHasActiveClassSet) {
      $currentPathExp = explode('/', $currentPath);
      $currentPathAliasExp = explode('/', $currentPathAlias);

      $currentPathFirstPart = $currentPathExp[1];
      $currentPathAliasFirstPart = $currentPathAliasExp[1];

      foreach ($tier1HeaderLinks as $tier1HeaderKey => $tier1HeaderValue) {
        $tier1HeaderValueUrl = parse_url($tier1HeaderValue['url'], PHP_URL_PATH);
        $tier1HeaderValueUrlExp = explode('/', $tier1HeaderValueUrl);

        if ($tier1HeaderValueUrlExp[1] == $currentPathFirstPart || $tier1HeaderValueUrlExp[1] == $currentPathAliasFirstPart) {
          $tier1HeaderLinks[$tier1HeaderKey]['current_page_classes'] = 'current-page tier-one-active-page';
          $alreadyHasActiveClassSet = true;
          break;
        }
      }

      // This path is not in the menu at all, so default the tier2 menu to the first tier1 link
      if (!$alreadyHasActiveClassSet) {
        $tier1HeaderLinks[0]['current_page_classes'] = 'current-page tier-one-active-page';
      }
    }

    return $tier1HeaderLinks;
  }

  /**
   * Return the current path
   *
   * @return string   The current path (or the site frontpage variable if this is the front page)
   */
  public function getCurrentPath() {
    $currentPath = $this->drupalGetRequestUri();
    // If this is the front page, return the path stored in the system site settings
    if ($currentPath === '/') {
      $config = $this->drupalGetConfig('system.site');
      return $config->get('page.front');
    }
    return $currentPath;
  }

  /**
   * Create an array of links for the tier2 header
   *
   * @param  string $currentPath         The current path
   * @param  string $currentPathAlias    The current path alias
   *
   * @return array
   *   The array of header menu links, ready for use in our twig template (header.html.twig)
   */
  public function getHeaderMenuLinks($currentPath, $currentPathAlias) {
    $tier1HeaderLinks = array();
    $tier2HeaderLinks = array();
    $headerMenuLinks = array();

    for ($i = 1; $i <= 7; $i++) {
      $tier1Link = array();
      $tier1link = $this->getLeftTier1Link($i);

      for ($j = 1; $j <= 5; $j++) {
        $tier2link = array();
        $tier2link['title'] = $this->drupalThemeGetSetting('tier1link' . $i . 'tier2title' . $j);
        $tier2link['url'] = $this->drupalThemeGetSetting('tier1link' . $i . 'tier2uri' . $j);
        $tier2link['parent_page_title'] = $this->drupalThemeGetSetting('lefttier1title' . $i);
        $tier2link['current_page_classes'] = '';
        // Only add the tier2link to the array if we actually have values
        if (!empty($tier2link['title']) && !empty($tier2link['url'])) {
          $linksWithActiveClasses = $this->addActiveClasses($currentPath, $currentPathAlias, $tier2link, $tier1link);
          $tier1link = $linksWithActiveClasses['tier1'];
          $tier2link = $linksWithActiveClasses['tier2'];
          $tier2HeaderLinks[] = $tier2link;
        }
      }
      // Only add the tier1link to the array if we actually have values
      if (!empty($tier1link['title']) && !empty($tier1link['url'])) {
        $tier1link = $this->addActiveClasses($currentPath, $currentPathAlias, $tier1link)['tier1'];
        $tier1HeaderLinks[] = $tier1link;
      }
    }
    $tier1HeaderLinks = $this->addClassesForNonMenuPages($currentPath, $currentPathAlias, $tier1HeaderLinks);
    $headerMenuLinks['tier1'] = $tier1HeaderLinks;
    $headerMenuLinks['tier2'] = $tier2HeaderLinks;

    return $headerMenuLinks;
  }

  /**
   * Create an array of right-aligned links for the tier1 header.
   *
   * @return array
   *   The array of right-aligned links, ready for use in our twig template (header.html.twig)
   */
  public function getRightTier1Links() {
    // Build the left_tier1_header_links array
    $rightTier1HeaderLinks = array();

    for ($i = 1; $i <= 2; $i++) {
      $link = array();
      $link['title'] = $this->drupalThemeGetSetting('righttier1title' . $i);
      $link['url'] = $this->drupalThemeGetSetting('righttier1uri' . $i);
      // Only add the link to the array if we actually have values
      if (!empty($link['title']) && !empty($link['url'])) {
        $rightTier1HeaderLinks[] = $link;
      }
    }

    return $rightTier1HeaderLinks;
  }

  /**
   * Create an array of links for the footer.
   *
   * @return array
   *   The array of footer links, ready for use in our twig template (footer.html.twig)
   */
  public function getFooterLinks() {
    $footerLinks = array();
    $defaultFooterLinks = $this->getDefaultFooterLinks();

    // Build the footer_links array
    for ($i = 1; $i <= count($defaultFooterLinks); $i++) {
      $link = array();
      $link['title'] = $this->drupalThemeGetSetting('footertitle' . $i);
      $link['url'] = $this->drupalThemeGetSetting('footeruri' . $i);
      // If the theme settings have not yet been set, return the defaults
      if (empty($link['title']) && empty($link['url'])) {
        $link = $defaultFooterLinks[$i];
      }
      $footerLinks[] = $link;
    }

    return $footerLinks;
  }

  /**
   * Get the content for the legal section of the footer.
   *
   * @return array
   *   The html for the legal footer div, ready for use in our twig template (footer.html.twig)
   */
  public function getLegalFooterContent() {
    $defaultLegalFooterContent = $this->getDefaultLegalFooterContent();

    return !empty($this->drupalThemeGetSetting('legalfooter')) ? $this->drupalThemeGetSetting('legalfooter') : $defaultLegalFooterContent;
  }

  /**
   * A helper method to get the default footer links values.
   *
   * @return array
   *   An array of the default footer link titles and urls
   */
  public function getDefaultFooterLinks() {
    $link1 = array('title' => 'Terms of Use', 'url' => 'https://www.principal.com/terms-of-use');
    $link2 = array('title' => 'Disclosures', 'url' => 'https://www.principal.com/products-services-disclosures');
    $link3 = array('title' => 'Privacy', 'url' => 'https://www.principal.com/privacy-policies');
    $link4 = array('title' => 'Security', 'url' => 'https://www.principal.com/security-policies');
    $link5 = array('title' => 'Report Fraud', 'url' => 'https://www.principal.com/about-us/our-company/policies/report-fraud-or-unethical-conduct');
    $link6 = array('title' => 'Site Map', 'url' => 'https://www.principal.com/site-map');
    $link7 = array('title' => '', 'url' => '');
    $defaultLinks = array(1 => $link1, 2 => $link2, 3 => $link3, 4 => $link4, 5 => $link5, 6 => $link6, 7 => $link7);

    return $defaultLinks;
  }

  /**
   * A helper method to get the default footer legal content.
   *
   * @return string
   *   A string containing the default footer legal content
   */
  public function getDefaultLegalFooterContent() {
    return '<p>© ' . $this->phpStrftime('%Y') . ', Principal Financial Services, Inc.</p><p>Securities offered through Principal Securities, Inc., <a href="http://www.sipc.org">member SIPC</a></p>';
  }

  /**
   * Build the footer theme settings form and set the values appropriately.
   *
   * @param  array $form   The form array, passed from horizon.theme
   *
   * @return array
   *   The $form array that was passed in, with the appropriate values set
   *
   * @see  horizon_form_system_theme_settings_alter
   */
  public function buildFooterThemeSettings($form) {
    $defaultLegalFooterContent = $this->getDefaultLegalFooterContent();
    $defaultFooterLinks = $this->getDefaultFooterLinks();

    // Checkbox to include social icons
    $form['includesocial'] = array(
      '#type'  => 'checkbox',
      '#title' => $this->drupalT('Include Social Icons?'),
      '#group' => 'footer',
      '#default_value' => $this->drupalThemeGetSetting('includesocial'),
      '#weight' => -10
    );
    // Link fields
    $form['footerlinks'] = array(
      '#type'  => 'details',
      '#title' => $this->drupalT('Footer Links'),
      '#open' => false,
      '#group' => 'footer',
      '#weight' => 0
    );

    for ($i = 1; $i <= 7; $i++) {
      // Default collapsed state to false
      $is_open = true;
      // Collapse the last link to save on page real estate
      if ($i > 6) {
        $is_open = false;
      }
      // Create the link fieldset
      $form['footerlink' . $i] = array(
        '#type'  => 'details',
        '#title' => $this->drupalT('Footer Link ' . $i),
        '#open' => $is_open,
        '#group' => 'footerlinks'
      );
      // Create the link title field
      $form['footertitle' . $i] = array(
        '#type'  => 'textfield',
        '#title' => $this->drupalT('Text for link ' . $i),
        '#default_value' => !empty($this->drupalThemeGetSetting('footertitle' . $i)) ? $this->drupalThemeGetSetting('footertitle' . $i) : $defaultFooterLinks[$i]['title'],
        '#group' => 'footerlink' . $i
      );
      // Create the link url field
      $form['footeruri' . $i] = array(
        '#type'  => 'textfield',
        '#title' => $this->drupalT('URL for link ' . $i),
        '#default_value' => !empty($this->drupalThemeGetSetting('footeruri' . $i)) ? $this->drupalThemeGetSetting('footeruri' . $i) : $defaultFooterLinks[$i]['url'],
        '#group' => 'footerlink' . $i
      );
    }
    // Text area for legal footer
    $form['legalfooter'] = array(
      '#type'  => 'textarea',
      '#title' => $this->drupalT('Legal Footer Content'),
      '#default_value' => !empty($this->drupalThemeGetSetting('legalfooter')) ? $this->drupalThemeGetSetting('legalfooter') : $defaultLegalFooterContent,
      '#group' => 'footer',
      '#weight' => 10
    );

    return $form;
  }

  /**
   * Build the co-branded logo theme settings form and set the values appropriately.
   *
   * @param  array $form    The form array, passed from horizon.theme
   *
   * @return array
   *   The $form array that was passed in, with the appropriate values set
   *
   * @see  horizon_form_system_theme_settings_alter()
   */
  public function buildCoBrandedLogoThemeSettings($form) {

    // Upload field for Co-branded logo
    $form['cobrand']['uploadlogo'] = array(
      '#title' => $this->drupalT('Tier 1 Co-Branded Logo Image'),
      '#type' => 'managed_file',
      '#description' => $this->drupalT('This logo will replace the Principal logo in the tier 1 header.'),
      '#default_value' => $this->drupalThemeGetSetting('uploadlogo'),
      '#upload_location' => $this->drupalFileDefaultScheme() . '://cobranded-logo/',
      '#group' => 'cobrand',
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      )
    );

    $form['cobrand']['uploadlogo2'] = array(
      '#title' => $this->drupalT('Tier 2 Co-Branded Logo Image'),
      '#type' => 'managed_file',
      '#description' => $this->drupalT('This logo will replace the Principal logo in the tier 2 header.'),
      '#default_value' => $this->drupalThemeGetSetting('uploadlogo2'),
      '#upload_location' => $this->drupalFileDefaultScheme() . '://cobranded-logo/',
      '#group' => 'cobrand',
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      )
    );

    return $form;
  }

  /**
   * Determine if we have a co-branded logo image for the tier1 header
   *
   * @return boolean
   *   True if the co-branded logo exists, false otherwise
   *
   * @see  horizon_preprocess_page
   */
  public function hasCoBrandedLogo() {
    return !is_null($this->drupalThemeGetSetting('uploadlogo')[0]) ? true : false;
  }

  /**
   * Determine if we have a co-branded logo image for the tier2 header
   *
   * @return boolean
   *   True if the co-branded logo exists, false otherwise
   *
   * @see  horizon_preprocess_page
   */
  public function hasCoBrandedLogo2() {
    return !is_null($this->drupalThemeGetSetting('uploadlogo2')[0]) ? true : false;
  }

  /**
   * Get the co-branded logo url, if it exists
   *
   * @return string
   *   The co-branded logo url if it exists, empty string otherwise
   *
   * @see  horizon_preprocess_page()
   */
  public function getCoBrandedLogoURL() {
    if ($this->hasCoBrandedLogo()) {
      $file = $this->drupalFileLoad($this->drupalThemeGetSetting('uploadlogo')[0]);
      $uriValue = isset($file->uri) ? $file->uri->getValue()[0]['value'] : '';
      return $this->drupalFileCreateUrl($uriValue);
    } else {
      return '';
    }
  }

  /**
   * Get the co-branded logo 2 url, if it exists
   *
   * @return string
   *   The co-branded logo url if it exists, empty string otherwise
   *
   * @see  horizon_preprocess_page
   */
  public function getCoBrandedLogo2URL() {
    if ($this->hasCoBrandedLogo2()) {
      $file = $this->drupalFileLoad($this->drupalThemeGetSetting('uploadlogo2')[0]);
      $uriValue = isset($file->uri) ? $file->uri->getValue()[0]['value'] : '';
      return $this->drupalFileCreateUrl($uriValue);
    } else {
      return '';
    }
  }

  /**
   * Build the form groups for our Horizon theme settings
   *
   * @param  array $form    The form array, passed from horizon.theme
   *
   * @return array
   *   The $form array that was passed in, with the appropriate values set
   *
   * @see  horizon_form_system_theme_settings_alter
   */
  public function buildHorizonSettingGroups($form) {
    // Add the 'Horizon' tabs group to the theme settings page
    $form['horizon'] = [
      '#type' => 'vertical_tabs',
      '#attached' => ['library' => ['horizon/theme-settings']],
      '#prefix' => '<h2><small>' . $this->drupalT('Horizon Settings') . '</small></h2>',
      '#weight' => -20
    ];
    $groups = [
      'header'  => $this->drupalT('Header'),
      'footer'  => $this->drupalT('Footer'),
      'cobrand' => $this->drupalT('Co-branded logo')
    ];
    foreach ($groups as $group => $title) {
      $form[$group] = [
        '#type' => 'details',
        '#title' => $title,
        '#group' => 'horizon'
      ];
    }

    return $form;
  }

  /**
   * Build the header form elements for our Horizon theme settings
   *
   * @param  array $form    The form array, passed from horizon.theme
   *
   * @return array
   *   The $form array that was passed in, with the appropriate values set
   *
   * @see  horizon_form_system_theme_settings_alter
   */
  public function buildHeaderThemeSettings($form) {
    // Create the left-aligned links fieldset
    $form['leftlinks'] = array(
      '#type'  => 'details',
      '#title' => $this->drupalT('Tier1 Header Links - Left Aligned'),
      '#open' => true,
      '#group' => 'header'
    );

    // Create the right-aligned links fieldset
    $form['rightlinks'] = array(
      '#type'  => 'details',
      '#title' => $this->drupalT('Tier1 Header Links - Right Aligned'),
      '#open' => true,
      '#group' => 'header'
    );

    // Create tier1 left-aligned header links fields (text and url)
    for ($i = 1; $i <= 7; $i++) {
      // Default collapsed state to false
      $is_open = true;
      // Collapse the last 5 links to save on page real estate
      if ($i > 3) {
        $is_open = false;
      }
      // Create the link fieldset
      $form['lefttier1link' . $i] = array(
        '#type'  => 'details',
        '#title' => $this->drupalT('Left Tier1 Header Link ' . $i),
        '#open' => $is_open,
        '#group' => 'leftlinks'
      );
      // Create the link title field
      $form['lefttier1title' . $i] = array(
        '#type'  => 'textfield',
        '#title' => $this->drupalT('Text for link ' . $i),
        '#default_value' => $this->drupalThemeGetSetting('lefttier1title' . $i),
        '#group' => 'lefttier1link' . $i
      );
      // Create the link url field
      $form['lefttier1uri' . $i] = array(
        '#type'  => 'textfield',
        '#title' => $this->drupalT('URL for link ' . $i),
        '#default_value' => $this->drupalThemeGetSetting('lefttier1uri' . $i),
        '#group' => 'lefttier1link' . $i
      );
      // Create tier2 link fields (text and url)
      for ($j = 1; $j <= 5; $j++) {
        $is_open = true;

        if ($j > 1) {
          $is_open = false;
        }
        // Create the link fieldset
        $form['tier1link' . $i . 'tier2link' . $j] = array(
          '#type'  => 'details',
          '#title' => $this->drupalT('Tier2 Header Link ' . $j),
          '#open' => $is_open,
          '#group' => 'lefttier1link' . $i
        );
        // Create the link title field
        $form['tier1link' . $i . 'tier2title' . $j] = array(
          '#type'  => 'textfield',
          '#title' => $this->drupalT('Text for link ' . $j),
          '#default_value' => $this->drupalThemeGetSetting('tier1link' . $i . 'tier2title' . $j),
          '#group' => 'tier1link' . $i . 'tier2link' . $j
        );
        // Create the link url field
        $form['tier1link' . $i . 'tier2uri' . $j] = array(
          '#type'  => 'textfield',
          '#title' => $this->drupalT('URL for link ' . $j),
          '#default_value' => $this->drupalThemeGetSetting('tier1link' . $i . 'tier2uri' . $j),
          '#group' => 'tier1link' . $i . 'tier2link' . $j
        );
      }
    }
    // Create tier1 right-aligned header links fields (text and url)
    for ($i = 1; $i <= 2; $i++) {
      // Default collapsed state to false
      $is_open = true;
      // Collapse the last link to save on page real estate
      if ($i > 1) {
        $is_open = false;
      }
      // Create the link fieldset
      $form['righttier1link' . $i] = array(
        '#type'  => 'details',
        '#title' => $this->drupalT('Right Tier1 Header Link ' . $i),
        '#open' => $is_open,
        '#group' => 'rightlinks'
      );
      // Create the link title field
      $form['righttier1title' . $i] = array(
        '#type'  => 'textfield',
        '#title' => $this->drupalT('Text for link ' . $i),
        '#default_value' => $this->drupalThemeGetSetting('righttier1title' . $i),
        '#group' => 'righttier1link' . $i
      );
      // Create the link url field
      $form['righttier1uri' . $i] = array(
        '#type'  => 'textfield',
        '#title' => $this->drupalT('URL for link ' . $i),
        '#default_value' => $this->drupalThemeGetSetting('righttier1uri' . $i),
        '#group' => 'righttier1link' . $i
      );
    }

    return $form;
  }

  /**
   * Determine if the current page has a full width hero, so we can adjust the container
   *
   * @param  array $variables    The variables array, passed from horizon.theme
   *
   * @return boolean    True if this is a hero page, false otherwise
   *
   * @see  horizon_preprocess_page() in horizon.theme
   */
  public function isMicrositeHeroPage($variables) {
    if (isset($variables['node'])) {
      if (is_string($variables['node'])) {
        $node = $this->drupalNodeLoad($variables['node']);
      } else {
        $node = $variables['node'];
      }
      if (isset($node->toArray()['ds_switch'][0]['value']) && $node->toArray()['ds_switch'][0]['value'] == 'microsites_hero') {
        return true;
      }
    }
    return false;
  }

  /**
   * Set the proper form element attribute classes
   *
   * @param  array $variables    The variables array, passed from horizon.theme
   *
   * @return array $variables    The variables array that was passed in, with our changes made to the attribute classes
   *
   * @see  horizon_preprocess_form_element() in horizon.theme and process() in \Drupal\bootstrap\Plugin\ProcessManager
   */
  public function setFormElementAttributeClasses($variables) {
    // Remove the form-inline class from form elements if it exists
    // Bootstrap adds this class in on certain field types, but we don't need it
    if (isset($variables['attributes']['class']) && in_array('form-inline', $variables['attributes']['class'])) {
      $key = array_search('form-inline', $variables['attributes']['class']);
      unset($variables['attributes']['class'][$key]);
    }
    // Add our form-group class to the form elements to pull in Horizon styling
    $variables['attributes']['class'][] = 'form-group';

    return $variables;
  }

  /**
   * Determine if the current path is an external url
   *
   * @return boolean    True if the current url is an external path, false otherwise
   *
   * @see  horizon_preprocess_html() in horizon.theme
   */
  public function isExternalPath() {
    $requestUri = $this->drupalGetRequestUri();
    $externalRequestUris = array('/external/footer', '/external/header');
    return in_array($requestUri, $externalRequestUris);
  }

  /**
   * Save a file from Drupal's $form_state
   * This function sets files to be stored permanently instead of as temp files
   *
   * @param  array    $formState        Drupal's $form_state array, passed from horizon.theme
   * @param  string   $formFieldName    The name of the form field that contains the file we want to save
   *
   * @return null   No return
   *
   * @see  horizon_form_system_theme_settings_submit() in horizon.theme
   */
  public function saveFileAsPermanent($formState, $formFieldName) {
    if (!empty($formState->getValue($formFieldName))) {
      $fidArray = $formState->getValue($formFieldName);
      $file = $this->drupalFileLoad($fidArray[0]);
      $file->setPermanent();
      $file->save();
    }
  }

}
