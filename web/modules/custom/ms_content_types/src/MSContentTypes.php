<?php
/**
 * @file
 * Contains \Drupal\ms_content_types\MSContentTypes.
 * Class for more complex and/or helper functions for the ms_content_types module.
 */

namespace Drupal\ms_content_types;

use Principal\traits\Construct;

class MSContentTypes {

  use Construct;

  /**
   * Alters all the node edit forms to make adjustments to the meta tag
   * and revision field handling.
   *
   * @param array $form
   *   The build array for the node edit form.
   *
   * @return array $form
   *   The form array that was passed in, with the appropriate adjustments made.
   */
  public function alterAllNodeEditForms($form, $formId) {
    $formKey = '';

    // Exclude webforms
    if ($formId === 'node_webform_form') {
      return $form;
    }

    // Set the revision log field to be required
    $form['revision_log']['widget'][0]['value']['#required'] = true;

    // prevent html5 validation
    $form['#attributes']['novalidate'] = '1';

    return $form;
  }

  /**
   * Customization for meta tags
   *
   * @param array $metatagsField
   *   The metatags field on the node edit form.
   *
   * @return array $metatagsField
   *   The field array that was passed in, with the appropriate adjustments made.
   */
  public function setPrincipalMetaTags($metatagsField) {
    // Mark the meta tags field as required, because we are requiring tags within the container
    $metatagsField['#required'] = true;

    // Set the 'description' and 'keywords' basic meta tags to required
    $metatagsField['basic']['description']['#required'] = true;
    $metatagsField['basic']['keywords']['#required'] = true;

    // Set the Principal Tags group to default to open and require the Retention Code field
    $metatagsField['principaltags']['#open'] = true;
    $metatagsField['principaltags']['#weight'] = 2;
    $metatagsField['principaltags']['retention']['#required'] = true;

    // Override the advanced group weight so it stays at the bottom
    $metatagsField['advanced']['#weight'] = 3;

    return $metatagsField;
  }

  /**
   * Switch our view mode based on the DS view mode switch value.
   * This function overwrites the one in DS, because of a bug found with DS and moderation states.
   *
   * @param string                  $viewMode   The view mode of the current node
   * @param Drupal\node\Entity\Node $node       The current node object
   *
   * @return array $viewMode    The view mode that was passed in, with changes made if the DS switch is set
   *
   * @see ms_content_types_entity_view_mode_alter() in ms_content_types.module and ds_switch_view_mode_entity_view_mode_alter() in ds_switch_view_mode.module
   * @see  GCBIA-469 for a full description of the issue this code fixes
   */
  public function switchViewMode($viewMode, $node) {
    // DS uses node_get_page($node) instead of $node->getType() == 'xxx' here, but this breaks
    // for some unknown reason if a user publishes a node and then creates a new draft.
    // Using $node->getType() instead should be safe enough for our purposes.
    if ($node->getEntityTypeId() == 'node' && $node->getType() == 'page' && !empty($node->ds_switch->value) && $viewMode == 'full') {
      $viewMode = $node->ds_switch->value;
    }

    return $viewMode;
  }

  /**
   * Alters the metatag fields present under the Basic fieldset
   * for the Call to Action content type
   *
   * @param array $form    Drupal's form array
   *
   * @return array $form   The form array that was passed in, with the appropriate adjustments made
   */
  public function alterCtaMetatagForm($form, $formId) {
    $formKey = '';

    // only run this code for call to action forms
    if ($formId === 'node_call_to_action_form' || $formId === 'node_call_to_action_edit_form') {
      if (isset($form['field_meta_tags']['widget'][0]['basic']['description'])) {
        unset($form['field_meta_tags']['widget'][0]['basic']['description']);
      }
      if (isset($form['field_meta_tags']['widget'][0]['basic']['abstract'])) {
        unset($form['field_meta_tags']['widget'][0]['basic']['abstract']);
      }
      if (isset($form['field_meta_tags']['widget'][0]['basic']['keywords'])) {
        unset($form['field_meta_tags']['widget'][0]['basic']['keywords']);
      }
    }

    return $form;
  }

}
