Microsites Content Types
==========
This module is for custom content type modifications on Principal sites.

No configuration necessary.  Just install, enable, and enjoy!
