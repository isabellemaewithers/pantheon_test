<?php
require_once 'src/modules/ms_content_types/src/MSContentTypes.php';

class MSContentTypesTest extends PHPUnit_Framework_TestCase {
  private $helper;

  /**
   * @beforeClass
   */
  public function setup() {
    $this->helper = Drupal\ms_content_types\MSContentTypes::getInstance();
  }

  public function testAlterAllNodeEditForms_happy() {
    $form = array();
    $formId = 'formID';
    $expectedForm = array();
    $expectedForm['revision_log']['widget'][0]['value']['#required'] = true;
    $expectedForm['#attributes']['novalidate'] = '1';

    $result = $this->helper->alterAllNodeEditForms($form, $formId);
    $this->assertSame($expectedForm, $result);
  }

  public function testAlterAllNodeEditForms_webform() {
    $form = array('Don\'t change me');
    $formId = 'node_webform_form';

    $result = $this->helper->alterAllNodeEditForms($form, $formId);
    $this->assertSame($form, $result);
  }

  public function testSetPrincipalMetaTags_happy() {
    $metatagsField = array();
    $metatagsField['#required'] = false;
    $metatagsField['basic']['description']['#required'] = false;
    $metatagsField['basic']['keywords']['#required'] = false;
    $metatagsField['principaltags']['#open'] = false;
    $metatagsField['principaltags']['#weight'] = 1;
    $metatagsField['principaltags']['retention']['#required'] = false;
    $metatagsField['advanced']['#weight'] = 2;

    $expectedField = array();
    $expectedField['#required'] = true;
    $expectedField['basic']['description']['#required'] = true;
    $expectedField['basic']['keywords']['#required'] = true;
    $expectedField['principaltags']['#open'] = true;
    $expectedField['principaltags']['#weight'] = 2;
    $expectedField['principaltags']['retention']['#required'] = true;
    $expectedField['advanced']['#weight'] = 3;

    $result = $this->helper->setPrincipalMetaTags($metatagsField);
    $this->assertSame($expectedField, $result);
  }

  public function testSwitchViewMode_ohHappyDay() {
    $viewMode = 'full';

    $dsSwitchValue = 'mickeyDaMouse';

    $mockDsSwitch = $this->getMockBuilder('MockDsSwitch')
                         ->setConstructorArgs(array($dsSwitchValue))
                         ->getMock();

    $node = $this->getMockBuilder('MockEntity')
                 ->setConstructorArgs(array($mockDsSwitch))
                 ->getMock();

    $node->expects($this->once())
         ->method('getEntityTypeId')
         ->will($this->returnValue('node'));

    $node->expects($this->once())
         ->method('getType')
         ->will($this->returnValue('page'));

    $expectedResult = 'mickeyDaMouse';

    $result = $this->helper->switchViewMode($viewMode, $node);
    $this->assertEquals($expectedResult, $result);
  }

  public function testSwitchViewMode_sad_entityTypeIdNotNode() {
    $viewMode = 'full';

    $dsSwitchValue = 'mickeyDaMouse';

    $mockDsSwitch = $this->getMockBuilder('MockDsSwitch')
                         ->setConstructorArgs(array($dsSwitchValue))
                         ->getMock();

    $node = $this->getMockBuilder('MockEntity')
                 ->setConstructorArgs(array($mockDsSwitch))
                 ->getMock();

    $node->expects($this->once())
         ->method('getEntityTypeId')
         ->will($this->returnValue('oopsie daisies'));

    $node->expects($this->never())
         ->method('getType');

    $expectedResult = 'full';

    $result = $this->helper->switchViewMode($viewMode, $node);
    $this->assertEquals($expectedResult, $result);
  }

  public function testSwitchViewMode_sad_nodeTypeNotPage() {
    $viewMode = 'full';

    $dsSwitchValue = 'mickeyDaMouse';

    $mockDsSwitch = $this->getMockBuilder('MockDsSwitch')
                         ->setConstructorArgs(array($dsSwitchValue))
                         ->getMock();

    $node = $this->getMockBuilder('MockEntity')
                 ->setConstructorArgs(array($mockDsSwitch))
                 ->getMock();

    $node->expects($this->once())
         ->method('getEntityTypeId')
         ->will($this->returnValue('node'));

    $node->expects($this->once())
         ->method('getType')
         ->will($this->returnValue('oopsie daisies'));

    $expectedResult = 'full';

    $result = $this->helper->switchViewMode($viewMode, $node);
    $this->assertEquals($expectedResult, $result);
  }

  public function testSwitchViewMode_sad_dsSwitchValueIsEmpty() {
    $viewMode = 'full';

    $dsSwitchValue = '';

    $mockDsSwitch = $this->getMockBuilder('MockDsSwitch')
                         ->setConstructorArgs(array($dsSwitchValue))
                         ->getMock();

    $node = $this->getMockBuilder('MockEntity')
                 ->setConstructorArgs(array($mockDsSwitch))
                 ->getMock();

    $node->expects($this->once())
         ->method('getEntityTypeId')
         ->will($this->returnValue('node'));

    $node->expects($this->once())
         ->method('getType')
         ->will($this->returnValue('page'));

    $expectedResult = 'full';

    $result = $this->helper->switchViewMode($viewMode, $node);
    $this->assertEquals($expectedResult, $result);
  }

  public function testSwitchViewMode_sad_viewModeNotFull() {
    $viewMode = 'oopsie daisies';

    $dsSwitchValue = 'mickeyDaMouse';

    $mockDsSwitch = $this->getMockBuilder('MockDsSwitch')
                         ->setConstructorArgs(array($dsSwitchValue))
                         ->getMock();

    $node = $this->getMockBuilder('MockEntity')
                 ->setConstructorArgs(array($mockDsSwitch))
                 ->getMock();

    $node->expects($this->once())
         ->method('getEntityTypeId')
         ->will($this->returnValue('node'));

    $node->expects($this->once())
         ->method('getType')
         ->will($this->returnValue('page'));

    $expectedResult = 'oopsie daisies';

    $result = $this->helper->switchViewMode($viewMode, $node);
    $this->assertEquals($expectedResult, $result);
  }

  public function testAlterCtaMetatagForm() {
    $form = array();
    $form['field_meta_tags']['widget'][0]['basic']['description'] = true;
    $form['field_meta_tags']['widget'][0]['basic']['abstract'] = true;
    $form['field_meta_tags']['widget'][0]['basic']['keywords'] = true;
    $formId = 'node_call_to_action_form';
    $expectedForm = array();
    $expectedForm['field_meta_tags']['widget'][0]['basic'] = array();
    $result = $this->helper->alterCtaMetatagForm($form, $formId);
    $this->assertSame($expectedForm, $result);
  }

  public function testAlterCtaMetatagForm_InvalidFormId() {
    $form = array();
    $form['field_meta_tags']['widget'][0]['basic']['description'] = true;
    $form['field_meta_tags']['widget'][0]['basic']['abstract'] = true;
    $form['field_meta_tags']['widget'][0]['basic']['keywords'] = true;
    $formId = 'formId';
    $expectedForm = $form;
    $result = $this->helper->alterCtaMetatagForm($form, $formId);
    $this->assertSame($expectedForm, $result);
  }

}

?>
