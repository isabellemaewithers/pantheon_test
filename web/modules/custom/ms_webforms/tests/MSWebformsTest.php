<?php
require_once 'src/modules/ms_webforms/src/MSWebforms.php';

class MSWebformsTest extends PHPUnit_Framework_TestCase {
  private $mockHelper;

  /**
   * @beforeClass
   */
  public function setup() {
    $this->mockHelper = $this->getMockBuilder('Drupal\ms_webforms\MSWebforms')
                      ->setMethods(array('drupalT', 'createMarkup', 'drupalGetPath'))
                      ->disableOriginalConstructor()
                      ->getMock();
  }

  public function testAlterAllWebformForms_happy() {
    $form = array();
    $form['#attributes']['novalidate'] = 'yesvalidate';
    $form['actions']['submit']['#attributes']['class'] = array('submit-class');
    $form['actions']['next']['#value'] = 'next value';
    $form['actions']['next']['#attributes']['class'] = array('next attributes');
    $form['actions']['previous']['#value'] = 'previous value';
    $form['actions']['previous']['#attributes']['class'] = array('previous attributes');
    $form['#prefix'] = 'prefix';
    $form['#attached']['library'] = array('attached libs');

    $formId = 'webform_formID';

    $expectedResult = $form;
    $expectedResult['#attributes']['novalidate'] = 'novalidate';
    $expectedResult['actions']['submit']['#attributes']['class'] = array('submit-class', 'btn-primary');
    $expectedResult['actions']['next']['#attributes']['class'] = array('next attributes', 'next');
    $expectedResult['actions']['previous']['#attributes']['class'] = array('previous attributes', 'prev', 'btn-link');
    $expectedResult['#prefix'] = '<div class="clearfix"><small class="is-required-header pull-right">Drupal Teed: Required</small></div>';
    $expectedResult['#attached']['library'] = array('attached libs', 'ms_webforms/ms-webforms');

    $this->mockHelper->expects($this->once())
                     ->method('drupalT')
                     ->with('Required')
                     ->will($this->returnValue('Drupal Teed: Required'));

    $result = $this->mockHelper->alterAllWebformForms($form, $formId);
    $this->assertSame($expectedResult, $result);
  }

  public function testAlterAllWebformForms_nodeAtEndFormId() {
    $form = array();
    $form['#attributes']['novalidate'] = 'yesvalidate';

    $formId = 'formID_node';

    $expectedResult = $form;

    $this->mockHelper->expects($this->never())
                     ->method('drupalT');

    $result = $this->mockHelper->alterAllWebformForms($form, $formId);
    $this->assertSame($expectedResult, $result);
  }

  public function testAlterAllWebformForms_nodeWebformFormId() {
    $form = array();
    $form['#attributes']['novalidate'] = 'yesvalidate';

    $formId = 'node_webform_';

    $expectedResult = $form;

    $this->mockHelper->expects($this->never())
                     ->method('drupalT');

    $result = $this->mockHelper->alterAllWebformForms($form, $formId);
    $this->assertSame($expectedResult, $result);
  }

  public function testAlterAllWebformForms_sad() {
    $form = array();
    $form['#attributes']['novalidate'] = 'yesvalidate';
    $form['actions']['next']['#attributes']['class'] = array('next attributes');
    $form['actions']['previous']['#attributes']['class'] = array('previous attributes');
    $form['#prefix'] = 'prefix';
    $form['#attached']['library'] = array('attached libs');

    $formId = 'webform_formID';

    $expectedResult = $form;
    $expectedResult['#attributes']['novalidate'] = 'novalidate';
    $expectedResult['#prefix'] = '<div class="clearfix"><small class="is-required-header pull-right">Drupal Teed: Required</small></div>';
    $expectedResult['#attached']['library'] = array('attached libs', 'ms_webforms/ms-webforms');

    $this->mockHelper->expects($this->once())
                     ->method('drupalT')
                     ->with('Required')
                     ->will($this->returnValue('Drupal Teed: Required'));

    $result = $this->mockHelper->alterAllWebformForms($form, $formId);
    $this->assertSame($expectedResult, $result);
  }

  public function testPreprocessFormElement_happy() {
    $variables = array();
    $variables['element'] = array();
    $variables['attributes']['class'] = array();
    $variables['description']['content'] = 'before';
    $variables['description_display'] = 'notafter';

    $mockMarkup = $this->getMockBuilder('MockMarkup')
                       ->setConstructorArgs(array())
                       ->getMock();
    $mockMarkup->expects($this->once())
               ->method('render')
               ->will($this->returnValue('hello'));

    $variables['element']['#errors'] = $mockMarkup;

    $this->mockHelper->expects($this->once())
                     ->method('createMarkup')
                     ->will($this->returnValue('zippideedooda'));

    $expectedResult = $variables;
    $expectedResult['attributes']['class'] = array('has-error');
    $expectedResult['description']['content'] = 'zippideedooda';
    $expectedResult['description_display'] = 'after';

    $result = $this->mockHelper->preprocessFormElement($variables);
    $this->assertSame($expectedResult, $result);
  }

  public function testPreprocessFormElement_errorsNotSet() {
    $variables = array();
    $variables['element'] = array();
    $variables['attributes']['class'] = array();
    $variables['description']['content'] = 'before';
    $variables['description_display'] = 'notafter';

    $this->mockHelper->expects($this->never())
                     ->method('createMarkup');

    $result = $this->mockHelper->preprocessFormElement($variables);
    $this->assertSame($variables, $result);
  }

  public function testPreprocessFormElement_errorsNull() {
    $variables = array();
    $variables['element'] = array();
    $variables['element']['#errors'] = null;
    $variables['attributes']['class'] = array();
    $variables['description']['content'] = 'before';
    $variables['description_display'] = 'notafter';

    $this->mockHelper->expects($this->never())
                     ->method('createMarkup');

    $result = $this->mockHelper->preprocessFormElement($variables);
    $this->assertSame($variables, $result);
  }

  public function testAlterThemeRegistry_happy() {
    $themeRegistry = array();
    $themeRegistry['webform_progress_bar']['path'] = 'test';

    $this->mockHelper->expects($this->once())
                     ->method('drupalGetPath')
                     ->with('module', 'ms_webforms')
                     ->will($this->returnValue('YAY'));

    $expectedResult = array();
    $expectedResult['webform_progress_bar']['path'] = 'YAY/templates';
    $result = $this->mockHelper->alterThemeRegistry($themeRegistry);
    $this->assertSame($expectedResult, $result);
  }

}

?>
