<?php
/**
 * @file
 * Contains \Drupal\ms_webforms\MSWebforms.
 * Class for more complex and/or helper functions for the ms_webforms module.
 */

namespace Drupal\ms_webforms;

use Principal\traits\Construct;
use Principal\traits\DrupalFunctions;
use Principal\traits\MarkupFunctions;

class MSWebforms {

  use Construct;
  use DrupalFunctions;
  use MarkupFunctions;

  /**
   * Form alter for all webform forms
   *
   * @param   $form       The form array, passed from hook_form_alter()
   * @param   $formId     The formId string, passed from hook_form_alter()
   *
   * @return   $form   The form array that was passed in, with our changes
   *
   * @see   ms_webforms_form_alter() in ms_webforms.module
   */
  public function alterAllWebformForms($form, $formId) {
    // Only run this function for webforms
    if (strpos($formId, 'webform_') === false || strpos($formId, 'node_') === 0) {
      return $form;
    }

    // Disable html5 validation on fields
    $form['#attributes']['novalidate'] = 'novalidate';

    // Horizon success btn & helper btn adjustments
    if (isset($form['actions']['submit']['#attributes']['class'])) {
      // Only add btn-primary if we don't already have a btn-xxx class set
      if (!preg_grep('/btn-\S+/', $form['actions']['submit']['#attributes']['class'])) {
        $form['actions']['submit']['#attributes']['class'][] = 'btn-primary';
      }
    }
    // Horizon markup
    if (isset($form['actions']['next']['#value'])) {
      $form['actions']['next']['#attributes']['class'][] = 'next';
    }
    if (isset($form['actions']['previous']['#value'])) {
      $form['actions']['previous']['#attributes']['class'][] = 'prev';
      $form['actions']['previous']['#attributes']['class'][] = 'btn-link';
    }

    // Horizon top required field markup
    $form['#prefix'] = '<div class="clearfix"><small class="is-required-header pull-right">' . $this->drupalT('Required') . '</small></div>';

    // Attach our css to the form
    $form['#attached']['library'][] = 'ms_webforms/ms-webforms';

    return $form;
  }

  /**
   * Preprocessor for form elements
   *
   * @param   $variables   The variables array, passed from hook_preprocess_form_element()
   *
   * @return  $variables   The variables array that was passed in, with our changes
   *
   * @see   ms_webforms_preprocess_form_element() in ms_webforms.module
   */
  public function preprocessFormElement($variables) {
    $element = $variables['element'];

    if (isset($element['#errors']) && !is_null($element['#errors'])) {
      $variables['attributes']['class'][] = 'has-error';
      $variables['description']['content'] = $this->createMarkup('<span class="has-error-message help-block">&nbsp;' . $element['#errors']->render() . '</span>');
      $variables['description_display'] = 'after';
    }

    return $variables;
  }

  /**
   * Alter the theme registry to use our file for webform_progress_bar
   *
   * @param   $themeRegistry   The themeRegistry array, passed from hook_theme_registry_alter()
   *
   * @return  $themeRegistry   The themeRegistry array that was passed in, with our changes
   *
   * @see   ms_webforms_theme_registry_alter() in ms_webforms.module
   */
  public function alterThemeRegistry($themeRegistry) {
    $themeRegistry['webform_progress_bar']['path'] = $this->drupalGetPath('module', 'ms_webforms') . '/templates';

    return $themeRegistry;
  }

}
