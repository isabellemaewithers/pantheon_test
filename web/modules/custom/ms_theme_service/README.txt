Microsites Theme Service
==========
This module enables the theme service functionality for microsites.
You can view the content at /external/header and /external/footer.
It relies on the Horizon theme's horizon_preprocess_html() function in horizon.theme to set the is_external
variable that is used in the html.html.twig file.

No configuration necessary.  Just install, enable, and enjoy!
