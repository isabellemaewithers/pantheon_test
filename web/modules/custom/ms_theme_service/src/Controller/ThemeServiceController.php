<?php

/**
 * @file
 * Contains \Drupal\ms_theme_service\Controller\ThemeServiceController
 */
namespace Drupal\ms_theme_service\Controller;

use Drupal\Core\Controller\ControllerBase;

class ThemeServiceController extends ControllerBase {

  public function getFooterMarkup() {

    return [
        '#theme' => 'page__externalfooter'
    ];
  }

  public function getHeaderMarkup() {

    return [
        '#theme' => 'page__externalheader'
    ];
  }

}
