
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module contains custom webform security modifications for Principal microsites.

REQUIREMENTS
------------

 * Drupal 8
 * Webforms Module (https://www.drupal.org/project/webform)

RECOMMENDED MODULES
-------------------
 * This module is handy when you want to test emails locally: https://www.drupal.org/project/development_environment

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.

CONFIGURATION
-------------

This module has no additional configuration.

TROUBLESHOOTING
---------------

  * Validate that both webforms and this module are installed and enabled.
  * Run drush cr.
  * Check the log files for any errors.

FAQ
---

MAINTAINERS
-----------

  * Keisha Hoeksema
