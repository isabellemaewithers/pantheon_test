<?php

namespace Drupal\ms_webform_secure\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Url;

/**
 * Provides a webform element for element attributes.
 *
 * @FormElement("ms_webform_secure_checkbox")
 */
class MSWebformSecureCheckbox extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return array(
      '#input' => true,
      '#process' => [
        [$class, 'processMsWebformSecureChecbox'],
      ],
      '#theme_wrappers' => ['form_element'],
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {

    return;
  }

  /**
   * Processes element attributes.
   */
  public static function processMsWebformSecureChecbox(&$element, FormStateInterface $form_state, &$complete_form) {
    $config = \Drupal::service('config.factory')->get('mswebform.secure')->get('element.settings');
    $values = $form_state->getValues();
    $field_name = $values['key'];

    $element['ms_webform_secure']['secure'] = [
      '#type' => 'checkbox',
      '#title' => t('Do not save this field to the database.'),
      '#description' => t('For sensitive data, e.g. SSN fields. (Data will still be sent in emails.)'),
      '#default_value' => $config[$field_name]['secure'] ? $config[$field_name]['secure'] : 0
    ];

    $element['#element_validate'] = [[get_called_class(), 'validateMSWebformSecureCheckbox']];

    return $element;
  }

  /**
   * Validates element attributes.
   */
  public static function validateMSWebformSecureCheckbox(&$element, FormStateInterface $form_state, &$complete_form) {
    $config = \Drupal::service('config.factory')->getEditable('mswebform.secure')->get('element.settings');
    $values = $form_state->getValues();

    $field_name = $values['key'];
    $config[$field_name] = array(
      'secure' => $values['secure']
    );

    \Drupal::service('config.factory')->getEditable('mswebform.secure')->set('element.settings', $config)->save();
  }

}
