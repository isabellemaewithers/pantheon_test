<?php
namespace Drupal\ms_webform_secure;

use Drupal\webform\Entity\WebformSubmission;
use Principal\traits\DrupalFunctions;
use Principal\traits\Construct;

class MSWebformSecure {

  use Construct;
  use DrupalFunctions;

  /**
   * Form alter for the webform component edit form
   * Adds the "Secure Settings" fieldset
   *
   * @param   $form       The form array, passed from hook_form_alter()
   * @param   $formState  The formState object of type Drupal\Core\Form\FormStateInterface, passed from hook_form_alter
   *
   * @return   $form   The form array that was passed in, with our fieldset added on allowed field types
   *
   * @see   ms_webform_secure_form_alter() in ms_webform_secure.module
   */
  public function getAlteredForm($form, $formState) {
    if (isset($formState->getBuildInfo()['callback_object'])) {
      // Only add this checkbox on certain field types
      $allowedTypes = array('textfield', 'textarea');
      if (isset($formState->getBuildInfo()['callback_object']->getElement()['#type']) && in_array($formState->getBuildInfo()['callback_object']->getElement()['#type'], $allowedTypes)) {

        // Provide the fieldset for secure fields on the element config form
        $form['properties']['ms_webform_secure'] = [
          '#type' => 'details',
          '#title' => $this->drupalT('Secure Settings'),
          '#weight' => 0
        ];
        // Add checkbox for saving to the database
        $form['properties']['ms_webform_secure']['secure'] = [
          '#type' => 'ms_webform_secure_checkbox',
          '#title' => $this->drupalT('Do not save this field to the database.'),
          '#title_display' => 'hidden'
        ];
      }
    }

    return $form;
  }

  /**
   * Remove values marked as "secure" from the entity so they are not saved to the database
   * This runs prior to the submission being saved
   *
   * @param   $entity   The entity object of type Drupal\Core\Entity\EntityInterface, passed from ms_webform_secure_entity_presave
   * @param   $post     The $_POST array, passed from ms_webform_secure_entity_presave
   *
   * @return   $returnVals    An array with two keys
   *                          post - The $_POST array, with the original data added to it
   *                          entity - The entity object that was passed in, with our secure values removed
   *
   * @see   ms_webform_secure_entity_presave() in ms_webform_secure.module
   */
  public function removeSecureValuesFromEntity($entity, $post) {
    $returnVals = array();
    $post['original_data'] = array();

    if ($this->isWebformSubmission($entity)) {
      $config = $this->getWebformSecureSettings();
      $dataOriginal = $entity->getData();
      $post['original_data'] = $dataOriginal;

      foreach ($dataOriginal as $key => $value) {
        if (isset($config[$key]['secure']) && $config[$key]['secure']) {
          unset($data[$key]);
        }
        else {
          $data[$key] = $value;
        }
      }
      $entity->setData($data);
    }

    $returnVals['post'] = $post;
    $returnVals['entity'] = $entity;

    return $returnVals;
  }

  /**
   * Helper function to get the field settings to determine if it is a secure field
   * We wrap the call in a private function so we can mock it for unit tests
   *
   * @return   The mswebform.secure config settings from the Drupal config factory
   *
   * @see   removeSecureValuesFromEntity()
   */
  public function getWebformSecureSettings() {
    return \Drupal::service('config.factory')->get('mswebform.secure')->get('element.settings');
  }

  /**
   * Add values marked as "secure" back to the entity so they are included in the email
   * This runs after the submission is saved, but prior to the email being sent
   *
   * @param   $entity   The entity object of type Drupal\Core\Entity\EntityInterface, passed from hook_entity_insert or hook_entity_update
   * @param   $post     The $_POST array, passed from hook_entity_insert or hook_entity_update
   *
   * @return  $entity   $entity - The entity object that was passed in, with our secure values added back
   *
   * @see   ms_webform_secure_entity_update() and ms_webform_secure_entity_insert() in ms_webform_secure.module
   */
  public function addSecureValuesBackToEntity($entity, $post) {
    // Only run this on WebformSubmissions, otherwise it will run when every node is saved/updated
    if ($this->isWebformSubmission($entity)) {
      // If our $post array contains the original webform submission data, set our entity data to this value
      if (!empty($post['original_data'])) {
        $entity->setData($post['original_data']);
      }
    }

    return $entity;
  }

  /**
   * Helper function to determine if an entity is a WebformSubmission
   *
   * @param   $entity   The entity object of type Drupal\Core\Entity\EntityInterface, passed from hook_entity_insert or hook_entity_update
   *
   * @return  boolean  True if this is a WebformSubmission, false otherwise
   *
   * @see   addSecureValuesBackToEntity()
   */
  public function isWebformSubmission($entity) {
    return $entity instanceof WebformSubmission;
  }

  /**
   * Add a honeypot field to webforms to block spam submissions
   *
   * @param   $form    The form array, passed from hook_form_alter()
   *
   * @return  $form    The form array that was passed in, with our honeypot field added
   *
   * @see   ms_webform_secure_form_alter() in ms_webform_secure.module
   */
  public function getAlteredSubmissionForm($form) {

    // Add 'autocomplete="off"' attribute.
    $attributes = array('autocomplete' => 'off');

    // Build the hidden element and use our custom validation function
    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => $this->drupalT('Leave this field blank.'),
      '#size' => 20,
      '#weight' => 100,
      '#attributes' => $attributes,
      '#element_validate' => array('_ms_webform_secure_hidden_field_validate'),
      '#prefix' => '<div class="hidden">',
      '#suffix' => '</div>'
    );

    return $form;
  }

  /**
   * Validation callback for anti-spam hidden form fields (honeypot fields)
   * This runs on any form field with _ms_webform_secure_honeypot_field_validate set as the '#element_validate' function
   *
   * @param   $element    The form element we are validating
   * @param   $formState  The formState object of type Drupal\Core\Form\FormStateInterface, passed from _ms_webform_secure_honeypot_field_validate
   *
   * @return  null    No return; we are setting an error on the formState object
   *
   * @see   _ms_webform_secure_honeypot_field_validate() in ms_webform_secure.module
   * @see   getAlteredSubmissionForm()
   */
  public function validateHoneypotField($element, &$formState) {
    // If the hidden field is not empty, log the failed spam attempt and throw a form error
    if (!empty($element['#value'])) {
      $message = 'Blocked submission of ' . $formState->getBuildInfo()['form_id'] . ' from IP address: ' . $this->drupalGetClientIP();
      $this->drupalLoggerNotice('ms_webform_secure', $message);

      $formState->setError($element, $this->drupalT('This field should be left blank.'));
    }
  }

}
