<?php
require_once 'src/modules/ms_webform_secure/src/MSWebformSecure.php';

class MSWebformSecureTest extends PHPUnit_Framework_TestCase {
  private $mockHelper;

  /**
   * @beforeClass
   */
  public function setup() {
    $this->mockHelper = $this->getMockBuilder('Drupal\ms_webform_secure\MSWebformSecure')
                      ->setMethods(array('drupalLoggerNotice','drupalGetClientIP', 'drupalT', 'isWebformSubmission', 'getWebformSecureSettings'))
                      ->disableOriginalConstructor()
                      ->getMock();
  }

  public function testValidateHoneypotField_happy() {
    $element = array();
    $element['#value'] = 'I can haz value?';
    $testBuildInfo = array();
    $testBuildInfo['form_id'] = 'i_can_haz_form_id';
    $message = 'Blocked submission of i_can_haz_form_id from IP address: dis iz my IP';

    $this->mockHelper->expects($this->once())
                     ->method('drupalGetClientIP')
                     ->will($this->returnValue('dis iz my IP'));

    $this->mockHelper->expects($this->once())
                     ->method('drupalLoggerNotice')
                     ->with('ms_webform_secure', $message);

    $this->mockHelper->expects($this->once())
                     ->method('drupalT')
                     ->with('This field should be left blank.')
                     ->will($this->returnValue('Drupal Teed: This field should be left blank.'));

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->once())
              ->method('getBuildInfo')
              ->will($this->returnValue($testBuildInfo));

    $formState->expects($this->once())
              ->method('setError')
              ->with($element, 'Drupal Teed: This field should be left blank.');

    $result = $this->mockHelper->validateHoneypotField($element, $formState);
  }

  public function testValidateHoneypotField_elementValueIsEmpty() {
    $element = array();
    $element['#value'] = '';

    $this->mockHelper->expects($this->never())
                     ->method('drupalGetClientIP');

    $this->mockHelper->expects($this->never())
                     ->method('drupalLoggerNotice');

    $this->mockHelper->expects($this->never())
                     ->method('drupalT');

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->never())
              ->method('getBuildInfo');

    $formState->expects($this->never())
              ->method('setError');

    $result = $this->mockHelper->validateHoneypotField($element, $formState);
  }

  public function testValidateHoneypotField_elementValueDoesNotExist() {
    $element = array();

    $this->mockHelper->expects($this->never())
                     ->method('drupalGetClientIP');

    $this->mockHelper->expects($this->never())
                     ->method('drupalLoggerNotice');

    $this->mockHelper->expects($this->never())
                     ->method('drupalT');

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->never())
              ->method('getBuildInfo');

    $formState->expects($this->never())
              ->method('setError');

    $result = $this->mockHelper->validateHoneypotField($element, $formState);
  }

  public function testGetAlteredSubmissionForm() {
    $form = array();
    $expectedResult = array();
    $expectedAttributes = array();
    $expectedUrlField = array();

    $expectedAttributes['autocomplete'] = 'off';

    $expectedUrlField['#type'] = 'textfield';
    $expectedUrlField['#title'] = 'Drupal Teed: Leave this field blank.';
    $expectedUrlField['#size'] = 20;
    $expectedUrlField['#weight'] = 100;
    $expectedUrlField['#attributes'] = $expectedAttributes;
    $expectedUrlField['#element_validate'] = array('_ms_webform_secure_hidden_field_validate');
    $expectedUrlField['#prefix'] = '<div class="hidden">';
    $expectedUrlField['#suffix'] = '</div>';

    $expectedResult['url'] = $expectedUrlField;

    $this->mockHelper->expects($this->once())
                     ->method('drupalT')
                     ->with('Leave this field blank.')
                     ->will($this->returnValue('Drupal Teed: Leave this field blank.'));

    $result = $this->mockHelper->getAlteredSubmissionForm($form);
    $this->assertEquals($expectedResult, $result);
  }

  public function testAddSecureValuesBackToEntity_happy() {
    $post = array();
    $post['original_data'] = 'not empty';

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->once())
               ->method('setData')
               ->with($post['original_data']);

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(true));

    $result = $this->mockHelper->addSecureValuesBackToEntity($mockEntity, $post);
  }

  public function testAddSecureValuesBackToEntity_notWebformSubmission() {
    $post = array();
    $post['original_data'] = 'not empty';

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->never())
               ->method('setData');

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(false));

    $result = $this->mockHelper->addSecureValuesBackToEntity($mockEntity, $post);
  }

  public function testAddSecureValuesBackToEntity_originalDataEmpty() {
    $post = array();
    $post['original_data'] = '';

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->never())
               ->method('setData');

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(true));

    $result = $this->mockHelper->addSecureValuesBackToEntity($mockEntity, $post);
  }

  public function testAddSecureValuesBackToEntity_originalDataNotSet() {
    $post = array();

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->never())
               ->method('setData');

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(true));

    $result = $this->mockHelper->addSecureValuesBackToEntity($mockEntity, $post);
  }

  public function testRemoveSecureValuesFromEntity_happy() {
    $post = array();
    $originalData = array();
    $originalData['field_one'] = array();
    $originalData['field_one']['secure'] = false;
    $originalData['field_two'] = array();
    $originalData['field_two']['secure'] = true;
    $originalData['field_three'] = array();
    $originalData['field_three']['secure'] = false;
    $mockConfig = $originalData;
    $newData = array();
    $newData['field_one'] = array();
    $newData['field_one']['secure'] = false;
    $newData['field_three'] = array();
    $newData['field_three']['secure'] = false;
    $expectedPostArray = array();
    $expectedPostArray['original_data'] = $originalData;

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->once())
               ->method('getData')
               ->will($this->returnValue($originalData));

    $mockEntity->expects($this->once())
               ->method('setData')
               ->with($newData);

    $expectedEntity = $mockEntity;

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(true));

    $this->mockHelper->expects($this->once())
                     ->method('getWebformSecureSettings')
                     ->will($this->returnValue($mockConfig));

    $result = $this->mockHelper->removeSecureValuesFromEntity($mockEntity, $post);
    $this->assertArrayHasKey('post', $result);
    $this->assertArrayHasKey('entity', $result);
    $this->assertEquals($expectedPostArray, $result['post']);
    $this->assertEquals($expectedEntity, $result['entity']);
  }

  public function testRemoveSecureValuesFromEntity_noSecureFields() {
    $post = array();
    $originalData = array();
    $originalData['field_one'] = array();
    $originalData['field_one']['secure'] = false;
    $originalData['field_two'] = array();
    $originalData['field_two']['secure'] = false;
    $originalData['field_three'] = array();
    $originalData['field_three']['secure'] = false;
    $mockConfig = $originalData;
    $expectedPostArray = array();
    $expectedPostArray['original_data'] = $originalData;

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->once())
               ->method('getData')
               ->will($this->returnValue($originalData));

    $mockEntity->expects($this->once())
               ->method('setData')
               ->with($originalData);

    $expectedEntity = $mockEntity;

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(true));

    $this->mockHelper->expects($this->once())
                     ->method('getWebformSecureSettings')
                     ->will($this->returnValue($mockConfig));

    $result = $this->mockHelper->removeSecureValuesFromEntity($mockEntity, $post);
    $this->assertArrayHasKey('post', $result);
    $this->assertArrayHasKey('entity', $result);
    $this->assertEquals($expectedPostArray, $result['post']);
    $this->assertEquals($expectedEntity, $result['entity']);
  }

  public function testRemoveSecureValuesFromEntity_notWebformSubmission() {
    $post = array();
    $expectedPostArray = array();
    $expectedPostArray['original_data'] = array();

    $mockEntity = $this->getMockBuilder('MockWebformSubmissionEntity')
                      ->setConstructorArgs(array())
                      ->getMock();

    $mockEntity->expects($this->never())
               ->method('getData');

    $mockEntity->expects($this->never())
               ->method('setData');

    $expectedEntity = $mockEntity;

    $this->mockHelper->expects($this->once())
                     ->method('isWebformSubmission')
                     ->with($mockEntity)
                     ->will($this->returnValue(false));

    $this->mockHelper->expects($this->never())
                     ->method('getWebformSecureSettings');

    $result = $this->mockHelper->removeSecureValuesFromEntity($mockEntity, $post);
    $this->assertArrayHasKey('post', $result);
    $this->assertArrayHasKey('entity', $result);
    $this->assertEquals($expectedPostArray, $result['post']);
    $this->assertEquals($expectedEntity, $result['entity']);
  }

  public function testGetAlteredForm_happyTextfield() {
    $testBuildInfo = array();
    $callbackObject = $this->getMockBuilder('MockWebformUiElementEditForm')
                           ->setConstructorArgs(array())
                           ->getMock();
    $elementArray = array('#type' => 'textfield');
    $callbackObject->expects($this->exactly(2))
                   ->method('getElement')
                   ->will($this->returnValue($elementArray));
    $testBuildInfo['callback_object'] = $callbackObject;
    $form = array();
    $form['properties'] = array();

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->exactly(3))
              ->method('getBuildInfo')
              ->will($this->returnValue($testBuildInfo));

    $expectedResult = $form;
    $expectedResult['properties']['ms_webform_secure'] = [
          '#type' => 'details',
          '#title' => 'Drupal Teed: Secure Settings',
          '#weight' => 0
        ];

    $expectedResult['properties']['ms_webform_secure']['secure'] = [
          '#type' => 'ms_webform_secure_checkbox',
          '#title' => 'Drupal Teed: Do not save this field to the database.',
          '#title_display' => 'hidden'
        ];

    $drupalTCalls = array(
      array('Secure Settings', 'Drupal Teed: Secure Settings'),
      array('Do not save this field to the database.', 'Drupal Teed: Do not save this field to the database.')
    );

    $this->mockHelper->expects($this->exactly(2))
         ->method('drupalT')
         ->will($this->returnValueMap($drupalTCalls));

    $result = $this->mockHelper->getAlteredForm($form, $formState);

    $this->assertEquals($expectedResult, $result);
  }

  public function testGetAlteredForm_happyTextarea() {
    $testBuildInfo = array();
    $callbackObject = $this->getMockBuilder('MockWebformUiElementEditForm')
                           ->setConstructorArgs(array())
                           ->getMock();
    $elementArray = array('#type' => 'textarea');
    $callbackObject->expects($this->exactly(2))
                   ->method('getElement')
                   ->will($this->returnValue($elementArray));
    $testBuildInfo['callback_object'] = $callbackObject;
    $form = array();
    $form['properties'] = array();

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->exactly(3))
              ->method('getBuildInfo')
              ->will($this->returnValue($testBuildInfo));

    $expectedResult = $form;
    $expectedResult['properties']['ms_webform_secure'] = [
          '#type' => 'details',
          '#title' => 'Drupal Teed: Secure Settings',
          '#weight' => 0
        ];

    $expectedResult['properties']['ms_webform_secure']['secure'] = [
          '#type' => 'ms_webform_secure_checkbox',
          '#title' => 'Drupal Teed: Do not save this field to the database.',
          '#title_display' => 'hidden'
        ];

    $drupalTCalls = array(
      array('Secure Settings', 'Drupal Teed: Secure Settings'),
      array('Do not save this field to the database.', 'Drupal Teed: Do not save this field to the database.')
    );

    $this->mockHelper->expects($this->exactly(2))
         ->method('drupalT')
         ->will($this->returnValueMap($drupalTCalls));

    $result = $this->mockHelper->getAlteredForm($form, $formState);

    $this->assertEquals($expectedResult, $result);
  }

  public function testGetAlteredForm_callbackObjectNotSet() {
    $testBuildInfo = array();
    $form = array();
    $form['properties'] = array();

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->once())
              ->method('getBuildInfo')
              ->will($this->returnValue($testBuildInfo));

    $this->mockHelper->expects($this->never())
         ->method('drupalT');

    $expectedResult = $form;
    $result = $this->mockHelper->getAlteredForm($form, $formState);

    $this->assertEquals($expectedResult, $result);
  }

  public function testGetAlteredForm_elementTypeNotSet() {
    $testBuildInfo = array();
    $callbackObject = $this->getMockBuilder('MockWebformUiElementEditForm')
                           ->setConstructorArgs(array())
                           ->getMock();
    $elementArray = array('type' => 'textarea');
    $callbackObject->expects($this->once())
                   ->method('getElement')
                   ->will($this->returnValue($elementArray));
    $testBuildInfo['callback_object'] = $callbackObject;
    $form = array();
    $form['properties'] = array();

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->exactly(2))
              ->method('getBuildInfo')
              ->will($this->returnValue($testBuildInfo));

    $this->mockHelper->expects($this->never())
         ->method('drupalT');

    $expectedResult = $form;
    $result = $this->mockHelper->getAlteredForm($form, $formState);

    $this->assertEquals($expectedResult, $result);
  }

  public function testGetAlteredForm_elementTypeNotInArray() {
    $testBuildInfo = array();
    $callbackObject = $this->getMockBuilder('MockWebformUiElementEditForm')
                           ->setConstructorArgs(array())
                           ->getMock();
    $elementArray = array('#type' => 'nottextarea');
    $callbackObject->expects($this->exactly(2))
                   ->method('getElement')
                   ->will($this->returnValue($elementArray));
    $testBuildInfo['callback_object'] = $callbackObject;
    $form = array();
    $form['properties'] = array();

    $formState = $this->getMockBuilder('MockFormState')
                      ->setConstructorArgs(array())
                      ->getMock();

    $formState->expects($this->exactly(3))
              ->method('getBuildInfo')
              ->will($this->returnValue($testBuildInfo));

    $expectedResult = $form;

    $this->mockHelper->expects($this->never())
         ->method('drupalT');

    $result = $this->mockHelper->getAlteredForm($form, $formState);

    $this->assertEquals($expectedResult, $result);
  }

}

?>
