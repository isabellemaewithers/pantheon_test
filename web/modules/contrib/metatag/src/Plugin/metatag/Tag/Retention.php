<?php

namespace Drupal\metatag\Plugin\metatag\Tag;

use \Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * Provides a plugin for the 'retention' meta tag.
 *
 * @MetatagTag(
 *   id = "retention",
 *   label = @Translation("Retention Code"),
 *   description = @Translation("Please refer to the following document for a list of available retention codes: <a href='https://inside.theprincipal.net/is/support/retention/docs/Retsched.pdf' target='_blank'>https://inside.theprincipal.net/is/support/retention/docs/Retsched.pdf</a>"),
 *   name = "retention",
 *   group = "principaltags",
 *   weight = 0,
 *   type = "label",
 *   secure = 0,
 *   multiple = FALSE
 * )
 */
class Retention extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
