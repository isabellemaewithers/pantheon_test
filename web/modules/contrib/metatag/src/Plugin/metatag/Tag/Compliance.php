<?php

namespace Drupal\metatag\Plugin\metatag\Tag;

use \Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * Provides a plugin for the 'compliance' meta tag.
 *
 * @MetatagTag(
 *   id = "compliance",
 *   label = @Translation("Compliance Number"),
 *   description = @Translation("First character is alphabetic (always starts with a t). Characters 2 through 7 are numeric. Characters 8 through 11 are alphanumeric. Example: t15012202bm"),
 *   name = "compliance",
 *   group = "principaltags",
 *   weight = 0,
 *   type = "label",
 *   secure = 0,
 *   multiple = FALSE
 * )
 */
class Compliance extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
