<?php

namespace Drupal\metatag\Plugin\metatag\Group;

/**
 * The principaltags group.
 *
 * @MetatagGroup(
 *   id = "principaltags",
 *   label = @Translation("Principal Metatags"),
 *   description = @Translation("Custom meta tags for Principal sites."),
 *   weight = 1
 * )
 */
class PrincipalTags extends GroupBase {
  // Inherits everything from Base.
}
